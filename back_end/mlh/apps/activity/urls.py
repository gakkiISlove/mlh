from rest_framework.urls import url
from rest_framework.routers import DefaultRouter
from . import views

urlpatterns = [
    url(r'^activities/(?P<activity_id>\d+)/apply$', views.ActivityApplyView.as_view())
]

router = DefaultRouter()
router.register('activities', views.ActivityViewSet, base_name='activities')

urlpatterns += router.urls
