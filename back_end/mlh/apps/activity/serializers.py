from rest_framework import serializers
from .models import Activity, ActivityApply


class ActivityIndexSerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = ('id' ,'title', 'start_time', 'city', 'status', 'image_url')

class ActivityDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        exclude = ('create_time', "update_time")


class ActivityApplySerializer(serializers.ModelSerializer):
    class Meta:
        model = ActivityApply
        exclude = ('create_time', "update_time")