from django.shortcuts import render
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet
from .models import Activity, ActivityApply
from . import serializers
from rest_framework.viewsets import GenericViewSet


class ActivityViewSet(ReadOnlyModelViewSet):

    queryset = Activity.objects.all().order_by("start_time")

    def perform_authentication(self, request):
        # 讲执行身份的认证函数关掉，自己来判断
        print("11")
        pass

    def get_serializer_class(self):
        if self.action == "list":
            return serializers.ActivityIndexSerializer
        else:
            return serializers.ActivityDetailSerializer

    def retrieve(self, request, *args, **kwargs):
        try:
            user = request.user
        except Exception:
            user = None
        acitivty_id = self.kwargs.get("pk")
        activity = Activity.objects.get(id=acitivty_id)
        serializer = self.get_serializer(activity)
        response_data = serializer.data
        if user is not None and user.is_authenticated:
            # 已经登陆
            try:
                activity_apply = ActivityApply.objects.filter(user=user, activity_id=acitivty_id)
            except Exception:
                pass
            if activity_apply:
                response_data["is_apply"] = True

        # 未登陆
        return Response(response_data)


class ActivityApplyView(GenericAPIView):
    serializer_class = serializers.ActivityApplySerializer

    def perform_authentication(self, request):
        # 讲执行身份的认证函数关掉，自己来判断
        pass

    def post(self, request, activity_id):
        try:
            user = request.user
        except Exception:
            print("用户没有登陆")
            return Response({"message":"请先登陆"}, status=status.HTTP_401_UNAUTHORIZED)
        try:
            activity = Activity.objects.get(id=activity_id, status=1)
        except Exception:
            return Response({"message":"活动已经过期"})
        try:
            activityapply = ActivityApply.objects.filter(user=user, activity=activity)
        except Exception:
            activityapply = None
        if activityapply:
            return Response({"message":"已经报名"}, status=status.HTTP_403_FORBIDDEN)
        result = ActivityApply.objects.create(user=user, activity=activity)
        if not result:
            return Response({"message":"create failed"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response({"message":"apply successed"}, status=status.HTTP_201_CREATED)
