from rest_framework import status
from rest_framework.generics import ListAPIView, RetrieveAPIView, GenericAPIView, CreateAPIView
from rest_framework.response import Response

from news.models import SectionModel, ColumnModel, NewsModel, CommentModel
from . import serializers
from .serializers import NewsCreateSerializer


# GET  /sections/
class SectionView(ListAPIView):
    """
    首页_展示版块
    """
    serializer_class = serializers.SectionSerializer
    queryset = SectionModel.objects.all().order_by('sort')


# GET /columns/
class ColumnView(ListAPIView):
    """
    首页_展示专栏
    """
    serializer_class = serializers.ColumnSerializer
    # 展示已经声和通过的专栏
    queryset = ColumnModel.objects.filter(status=1).order_by('sort')

    def post(self, request):
        request_dict = request.data
        request_dict["sort"] = 10
        serializer = self.get_serializer(data=request_dict)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


# GET /news_list/?column_id=xxx
class NewsListView(ListAPIView):
    """
    首页_展示新闻列表以及专栏新闻切换
    """
    serializer_class = serializers.NewsListSerializer

    def get_queryset(self):
        column_id = self.request.query_params['column_id']
        if int(column_id) == 1:
            return NewsModel.objects.all().order_by('hit_count', '-create_time')
        else:
            return NewsModel.objects.filter(column_id=column_id)


# GET  /news/xx
class NewsDetailView(RetrieveAPIView):
    """
    获取新闻详细
    """
    serializer_class = serializers.NewsSerializer
    queryset = NewsModel.objects.all()

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        news_id = kwargs['pk']
        news = NewsModel.objects.get(id=news_id)
        news.hit_count += 1
        news.save()
        return Response(serializer.data)


# GET /comments/?news_id=xxx
class NewsCommentsView(ListAPIView):
    """
    展示新闻详情的评论
    """
    serializer_class = serializers.CommentsSerializer

    def get_queryset(self):
        news_id = self.request.query_params['news_id']
        news = NewsModel.objects.filter(id=news_id)
        return CommentModel.objects.filter(news__in=news, parent__isnull=True)


class AddNewsCommentsView(GenericAPIView):
    """
    添加评论
    """
    queryset = CommentModel.objects.all()
    serializer_class = serializers.AddCommentsSerializer

    def post(self, request):
        user_id = request.user.id
        news_id = int(request.data['news_id'])
        comment = request.data['comment']
        parent_id = int(request.data.get('parent_id'))
        if not user_id:
            return Response({'message': '请先登录'}, status=status.HTTP_403_FORBIDDEN)
        if not parent_id:
            data = {
                'user_id': user_id,
                'news_id': news_id,
                'comment': comment,
            }
        data = {
            'user_id': user_id,
            'news_id': news_id,
            'comment': comment,
            'parent_id': parent_id
        }
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


class AddNews(CreateAPIView):
    """
    添加新闻
    """
    serializer_class = NewsCreateSerializer
    queryset = NewsModel.objects.all()


