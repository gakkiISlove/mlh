from rest_framework import serializers

from users.models import User
from users.serializers import CreateUserSerializer
from . import models


class SectionSerializer(serializers.ModelSerializer):
    """
    序列化器_版块展示
    """
    class Meta:
        model = models.SectionModel
        fields = ('id', 'name', 'sort', 'url')


class ColumnSerializer(serializers.ModelSerializer):
    """
    序列化器_专栏展示
    """
    class Meta:
        model = models.ColumnModel
        fields = ('id', 'name', 'sort', 'url', "introduce")


class NewsListSerializer(serializers.ModelSerializer):
    """
    序列化器_新闻列表展示最新
    """
    create_time = serializers.DateTimeField(read_only=True, label='创建时间')
    author = CreateUserSerializer()

    class Meta:
        model = models.NewsModel
        fields = ('id', 'title', 'author', 'digest', 'create_time')


class ChildSerializer(serializers.ModelSerializer):
    """
    新闻评论的评论展示
    """
    user = CreateUserSerializer()

    class Meta:
        model = models.CommentModel
        fields = ('id', 'user', 'comment', 'comment_count')


class CommentsSerializer(serializers.ModelSerializer):
    """
    新闻评论展示
    """
    user = CreateUserSerializer()
    child_comments = ChildSerializer(many=True)

    class Meta:
        model = models.CommentModel
        fields = ('id', 'user', 'comment', 'comment_count', 'child_comments')


class NewsSerializer(serializers.ModelSerializer):
    """
    序列化器_新闻详细
    """
    author = CreateUserSerializer()

    class Meta:
        model = models.NewsModel
        fields = ('id',  'author', 'title', 'content', 'create_time', 'comment_count')


class AddCommentsSerializer(serializers.Serializer):
    """
    添加新闻评论
    """
    user_id = serializers.IntegerField(label='用户id')
    news_id = serializers.IntegerField(label='新闻id')
    parent_id = serializers.IntegerField(label='父评论id', required=False)
    comment = serializers.CharField(label='评论内容')

    def validate(self, attrs):
        news_id = attrs['news_id']
        parent_id = attrs.get('parent_id')
        if parent_id:
            try:
                news = models.NewsModel.objects.get(id=news_id)
            except models.NewsModel.DoesNotExist:
                raise serializers.ValidationError('出错啦！')

            try:
                comment = models.CommentModel.objects.get(id=parent_id)
            except models.CommentModel:
                raise serializers.ValidationError('出错啦！')

            if news == comment.news:
                return attrs
            else:
                serializers.ValidationError('出错啦！')
        try:
            news = models.NewsModel.objects.get(id=news_id)
        except models.NewsModel.DoesNotExist:
            raise serializers.ValidationError('出错啦！')
        return attrs

    def create(self, validated_data):
        user_id = validated_data['user_id']
        news_id = validated_data['news_id']
        comment = validated_data['comment']
        parent_id = validated_data.get('parent_id')
        if parent_id:
            models.CommentModel.objects.create(
                user_id=user_id,
                news_id=news_id,
                comment=comment,
                parent_id=parent_id
            )
            news = models.NewsModel.objects.get(id=news_id)
            news.comment_count += 1
            news.save()
            comment = models.CommentModel.objects.get(id=parent_id)
            comment.comment_count += 1
            comment.save()
            return validated_data
        models.CommentModel.objects.create(
            user_id=user_id,
            news_id=news_id,
            comment=comment,
        )
        news = models.NewsModel.objects.get(id=news_id)
        news.comment_count += 1
        news.save()
        return validated_data


class NewsCreateSerializer(serializers.ModelSerializer):
    """
    添加新闻
    """

    class Meta:
        model = models.NewsModel
        fields = ("id", "column", 'title', "author", "content", 'digest')
        extra_kwargs = {
            'digest': {'read_only': True}
        }

    def create(self, validated_data):
        print(validated_data)
        content = validated_data['content']
        title = validated_data['title']
        author = validated_data['author']
        column = validated_data['column']
        news = models.NewsModel.objects.create(
            column_id=column.id,
            title=title,
            author_id=author.id,
            content=content,
            digest=content[0:90] + '......'
        )
        return news




























        

