from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^sections/$', views.SectionView.as_view()),  # 首页，展示版块
    url(r'^columns/$', views.ColumnView.as_view()),  # 首页，展示专栏
    url(r'^news_list$', views.NewsListView.as_view()),  # 首页，展示新闻列表以及专栏切换新闻
    url(r'^news/(?P<pk>\d+)/$', views.NewsDetailView.as_view()),  # 新闻详情
    url(r'^comments/$', views.NewsCommentsView.as_view()),  # 新闻评论展示
    url(r'^news/$', views.AddNews.as_view()),  # 添加新闻
    url(r'^news_comment/$', views.AddNewsCommentsView.as_view()),  # 添加新闻评论
]
