from django.contrib import admin
from . import  models
# Register your models here.


admin.site.register(models.SectionModel)  # 版块
admin.site.register(models.ColumnModel)  # 专栏
admin.site.register(models.NewsModel)  # 新闻
admin.site.register(models.CommentModel)  # 评论


