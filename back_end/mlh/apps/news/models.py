from ckeditor.fields import RichTextField
from django.db import models

from mlh.utils.models import BaseModel
from users.models import User
from language.models import LanguageCategory


class SectionModel(BaseModel):
    """
    项目版块
    """
    name = models.CharField(max_length=16, verbose_name='名称')
    sort = models.IntegerField(verbose_name='排序')
    url = models.CharField(verbose_name='版块首页html文件名', max_length=32)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'tb_section'
        verbose_name = '项目版块'
        verbose_name_plural = verbose_name


class ColumnModel(BaseModel):
    """
    头条专栏
    """
    COLUMN_STATUS = (
        (-1, '未通过'),
        (0, '待审核'),
        (1, '已通过')
    )

    name = models.CharField(max_length=16, verbose_name='名称')
    sort = models.IntegerField(verbose_name='排序')
    url = models.CharField(max_length=64, verbose_name='网址')
    introduce = models.CharField(max_length=128, verbose_name='简介')
    status = models.SmallIntegerField(choices=COLUMN_STATUS, default=1, verbose_name='状态')

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'tb_column'
        verbose_name = '头条专栏'
        verbose_name_plural = verbose_name


class NewsModel(BaseModel):
    """"
    头条新闻
    """
    column = models.ForeignKey(ColumnModel, on_delete=models.PROTECT, verbose_name='专栏id', null=True, blank=True)
    title = models.CharField(max_length=64, verbose_name='标题')
    language = models.ManyToManyField(LanguageCategory, verbose_name='语言标签', default=1)
    author = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='作者')
    content = RichTextField(verbose_name='新闻内容')
    image = models.ImageField(verbose_name='新闻图片', upload_to='news', null=True, blank=True)
    comment_count = models.IntegerField(verbose_name='评论总数', default=0)
    digest = models.CharField(max_length=106, verbose_name='新闻摘要')
    hit_count = models.IntegerField(verbose_name='点击量', default=0)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'tb_news'
        verbose_name = '新闻'
        verbose_name_plural = verbose_name


class CommentModel(BaseModel):
    """
    新闻评论
    """
    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='评论员', null=True, blank=True)
    news = models.ForeignKey(NewsModel, on_delete=models.PROTECT, verbose_name='新闻id', related_name='comments', null=True, blank=True)
    parent = models.ForeignKey('self', on_delete=models.PROTECT, verbose_name='父评论id', null=True, blank=True, related_name='child_comments')
    comment = models.CharField(max_length=128, verbose_name='评论内容')
    comment_count = models.IntegerField(verbose_name='获得的总评论数', default=0)

    def __str__(self):
        return self.comment

    class Meta:
        db_table = 'tb_news_comment'
        verbose_name = '新闻评论'
        verbose_name_plural = verbose_name


class News2Language(models.Model):
    """
    新闻与语言标签
    """
    nid = models.ForeignKey(NewsModel, to_field="id", null=True, blank=True)
    lid = models.ForeignKey(LanguageCategory, to_field="id", null=True, blank=True)

    class Meta:
        unique_together = ("nid", "lid")


# TODO 暂且搁置
class UserFansModel(models.Model):
    """
    用户与粉丝
    """
    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='users', verbose_name='用户')
    fans = models.ForeignKey(User, on_delete=models.PROTECT, related_name='fans', verbose_name='粉丝')

    class Meta:
        db_table = 'tb_user_fans'
        verbose_name = '用户与粉丝'
        verbose_name_plural = verbose_name




