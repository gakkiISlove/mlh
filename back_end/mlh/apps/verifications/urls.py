from django.conf.urls import url
from rest_framework.routers import DefaultRouter

from .import views

urlpatterns = [

    url(r'^image_codes/(?P<image_code_id>[\w-]+)/$', views.ImageCodeView.as_view()),
    url(r'^sms_codes/(?P<mobile>1[3-9]\d{9})/$', views.SMSCodeView.as_view()),

]

# urlpatterns = []
#
# router = DefaultRouter()
# router.register('sms_codes', views.SMSCodeView, base_name='activities')
#
# urlpatterns += router.urls
