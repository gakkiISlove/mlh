import random

from django.http import HttpResponse
from rest_framework.generics import GenericAPIView
from rest_framework.views import APIView

from celery_tasks.sms import tasks as sms_tasks
from django_redis import get_redis_connection
from rest_framework.response import Response


from mlh.apps.verifications import constants
from mlh.libs.captcha.captcha import captcha


class ImageCodeView(APIView):
    """图片验证码"""

    def get(self, request, image_code_id):
        """
        生成验证图片
        保存真实值
        返回图片
        """
        # 生成验证图片
        text, image = captcha.generate_captcha()
        print(text)
        # 保存真实值
        redis_conn = get_redis_connection("verify_codes")
        redis_conn.setex("img_%s" % image_code_id, constants.IMAGE_CODE_REDIS_EXPIRES, text)

        # 返回图片
        # 固定返回验证码图片数据，不需要REST framework框架的Response帮助我们决定返回响应数据的格式
        # 所以此处直接使用Django原生的HttpResponse即可
        return HttpResponse(image, content_type="image/jpg")



class SMSCodeView(GenericAPIView):

    def get(self, request, mobile):
        """创建短信验证码"""
        sms_code = '%06d' % random.randint(0, 999999)
        print(sms_code)

        # 保存短信验证码与发送验证码
        redis_conn = get_redis_connection('verify_codes')
        pl = redis_conn.pipeline()
        pl.setex("sms_%s" % mobile, constants.SMS_CODE_REDIS_EXPIRES, sms_code)
        pl.setex("send_flag_%s" % mobile, constants.SEND_SMS_CODE_INTERVAL, 1)
        pl.execute()

        # 发送短信验证码
        sms_code_expires = str(constants.SMS_CODE_REDIS_EXPIRES // 60)
        sms_tasks.send_sms_code.delay(mobile, sms_code, sms_code_expires)

        return Response({"message": "OK"})
