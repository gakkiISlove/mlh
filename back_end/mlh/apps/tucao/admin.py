from django.contrib import admin
from .models import Spit, SpitComment, SpitLike, SpitCollection, CommentLike


admin.site.register(Spit)
admin.site.register(SpitComment)
admin.site.register(SpitLike)
admin.site.register(SpitCollection)
admin.site.register(CommentLike)
# admin.site.register(HeroIn