from django.apps import AppConfig


class LanguageConfig(AppConfig):
    name = 'language'
    verbose_name = '问答管理'