from rest_framework import serializers
from django.db import transaction
from .models import LanguageCategory,Question,Label,Answer,User2LanguageCategory
from users.models import User


class LabelSerializer(serializers.ModelSerializer):
    # 标签
    class Meta:
        model = Label
        fields = ('name',"id")


class LanguageCategorySerializer(serializers.ModelSerializer):
    # 语言类别分类
    label_set = LabelSerializer(many=True)
    class Meta:
        model = LanguageCategory
        fields = ('id','name','details','encyclopedias','label_set','attention')


class UsersSerializer(serializers.ModelSerializer):
    # 个人用户
    class Meta:
        model = User
        fields = ('username',)


class AnswerQuestionSerializer(serializers.ModelSerializer):
    # 回答
    user = UsersSerializer()
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Answer
        fields = ('id','create_time','answer_content','answer_usefuls','user')



class QuestionAnswerSerializer(serializers.ModelSerializer):
    # 提问
    label = LabelSerializer()
    user = UsersSerializer()
    language_category = LanguageCategorySerializer()
    answer_set = AnswerQuestionSerializer(many=True)

    class Meta:
        model = Question
        fields = ('id','label','title','solve','usefuls','answers','browses','create_time','update_time','content','user','language_category','answer_set')


class CategorySerializer(serializers.ModelSerializer):
    # 语言类别分类
    class Meta:
        model = LanguageCategory
        fields = ('id',)


class LabelCreateSerializer(serializers.ModelSerializer):
    # 标签
    class Meta:
        model = Label
        fields = ("id","name")


class QuestioncCreateSerializer(serializers.ModelSerializer):
    # 问题提交
    # language_category = CategorySerializer()
    language_category = serializers.PrimaryKeyRelatedField(queryset=LanguageCategory.objects.all())
    label = serializers.PrimaryKeyRelatedField(queryset=Label.objects.all())

    class Meta:
        model = Question
        fields = ('user','content','title','language_category','label')


class AnswerSerializer(serializers.ModelSerializer):
    # 回答
    question = QuestionAnswerSerializer()
    class Meta:
        model = Answer
        fields = ('answer_content','answer_usefuls','question')


class UsersFollowSerializer(serializers.ModelSerializer):
    # 个人用户
    class Meta:
        model = User
        fields = ('id',)


class LanguageFollowSerializer(serializers.ModelSerializer):
    # 语言关注展示
    user = UsersFollowSerializer(write_only=True)

    class Meta:
        model = LanguageCategory
        fields = ('id','name','attention','user','follow')


class AnswerCreateSerializer(serializers.ModelSerializer):
    """
    提交回答
    """
    class Meta:
        model = Answer
        fields = ('user','question','answer_content')


class AddFollowCreateSerializer(serializers.ModelSerializer):
    # 添加关注标签
    class Meta:
        model = User2LanguageCategory
        fields = "__all__"


class DelFollowCreateSerializer(serializers.ModelSerializer):
    # 取消关注标签
    class Meta:
        model = User2LanguageCategory
        fields = "__all__"
