from django.contrib import admin

# Register your models here.

from .models import LanguageCategory,Question,Label,Answer

admin.site.register(LanguageCategory)
admin.site.register(Question)
admin.site.register(Label)
admin.site.register(Answer)