from rest_framework.urls import url
from rest_framework.routers import DefaultRouter
from . import views



urlpatterns = [
    # 问答首页展示
    url(r'^language/$', views.Language_Banner.as_view()),
    # 问答首页点击排序展示
    url(r'^sorting/$', views.SortingView.as_view()),
    # 问答详情页展示
    url(r'^questiondetails/$', views.Question_details.as_view()),
    # 所有标签展示
    url(r'^customtag/$', views.CustomTagView.as_view()),


    # 问答详情页问题有用点击
    url(r'^useful/$', views.Usefulness.as_view()),
    # 问答详情页回答有用点击
    url(r'^answeruseful/$', views.AnswerUseful.as_view()),


    # 提交问题
    url(r'^submit/$', views.SubmitView.as_view()),
    # 提交回答
    url(r'answer/$',views.AnswerCreateView.as_view()),


    # 显示关注语言
    url(r'^follow/$',views.LanguageFollowView.as_view()),
    # 点击关注
    url(r'addfollow/$',views.AddFollowView.as_view()),
    # 取消关注deffollow
    url(r'delfollow/$',views.DelFollowView.as_view()),
    # 根据语言id查询是否被关注
    url(r'^queryfollow/$', views.QueryTagViewView.as_view()),



]
