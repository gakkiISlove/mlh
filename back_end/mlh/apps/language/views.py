from django.shortcuts import render
import operator
from rest_framework.generics import  CreateAPIView
# Create your views here.

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import mixins
from rest_framework import request
from rest_framework.generics import ListAPIView,GenericAPIView

from .models import LanguageCategory, Question, Label,Answer,User2LanguageCategory
from .serializers import *
from . import content
from users.models import User


def tablelist(list_a):
    """根据标签出现次数由多到少排序"""
    # list_a = [1, 2, 3, 4, 5, 6, 1, 1, 1, 2, 2, 3, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6]

    set_a = list(set(list_a))  # 去重得到一个集合
    count_set_a = {}  # 存放元素和出现次数的字典，key为元素,value为出现次数
    for item in set_a:
        count_set_a[item] = list_a.count(item)
    # 将cou_set_a按value值排序，返回一个list，list中元素是形式为(1,4)的tuple，tuple[0]为键值，tuple[1]为出现次数
    sorted_list_a = sorted(count_set_a.items(), key=operator.itemgetter(1))

    result_a = []  # 存放最后的结果
    for item in sorted_list_a[::-1]:  # 按value值从大到小排序
        result_a.append(item[0])
    return result_a[0:content.NUMBER_OF_LABELS]


class SortingView(APIView):
    """问答首页点击排序展示"""
    def get(self, request):
        cat = request.query_params.get('new')
        query = request.query_params.get('id')
        if query == None or query == '1':
            if cat == '1':
                lan = Question.objects.order_by('-update_time').all()
                s = QuestionAnswerSerializer(lan, many=True)
                return Response({"s": s.data})
            elif cat == '2':
                lan = Question.objects.order_by('-answers').all()
                s = QuestionAnswerSerializer(lan, many=True)
                return Response({"s": s.data})
            else:
                lan = Question.objects.order_by('answers').all()
                s = QuestionAnswerSerializer(lan, many=True)
                return Response({"s": s.data})
        elif int(query) in range(2, 10):
            if cat == '1':
                lan = Question.objects.order_by('-update_time').filter(language_category=query)
                s = QuestionAnswerSerializer(lan, many=True)
                return Response({"s": s.data})
            elif cat == '2':
                lan = Question.objects.order_by('-answers').filter(language_category=query)
                s = QuestionAnswerSerializer(lan, many=True)
                return Response({"s": s.data})
            else:
                lan = Question.objects.order_by('answers').filter(language_category=query)
                s = QuestionAnswerSerializer(lan, many=True)
                return Response({"s": s.data})


class Language_Banner(APIView):
    """
    问答首页展示
    """
    def get(self, request):
        query = request.query_params.get('id')
        new = request.query_params.get('new')

        data = LanguageCategory.objects.all()
        serializer = LanguageCategorySerializer(data, many=True)

        question_dict = Question.objects.all()
        question_data = QuestionAnswerSerializer(question_dict,many=True)


        label_list=[]
        for question in question_data.data:
            label_list.append(str(question['label']['name']))
        # 根据问题标签出现次数排序
        result_a = tablelist(label_list)

        if query == None or query == '1':
            if new == '1':
                lan = Question.objects.all().order_by('-update_time')
                s = QuestionAnswerSerializer(lan, many=True)
            elif new == '2':
                lan = Question.objects.all().order_by('-answers')
                s = QuestionAnswerSerializer(lan, many=True)
            elif new =='3':
                lan = Question.objects.all().order_by('answers')
                s = QuestionAnswerSerializer(lan, many=True)
            else:
                lan = Question.objects.order_by('-update_time').all()
                s = QuestionAnswerSerializer(lan, many=True)

        elif int(query) in range(2, 10):
            if new == '1':
                lan = Question.objects.filter(language_category=query).order_by('-update_time')
                s = QuestionAnswerSerializer(lan, many=True)
            elif new == '2':
                lan = Question.objects.filter(language_category=query).order_by('-answers')
                s = QuestionAnswerSerializer(lan, many=True)
            elif new == '3':
                lan = Question.objects.filter(language_category=query).order_by('answers')
                s = QuestionAnswerSerializer(lan, many=True)
            else:
                lan = Question.objects.order_by('-update_time').all()
                s = QuestionAnswerSerializer(lan, many=True)
        else:
            lan = Question.objects.order_by('-update_time').all()
            s = QuestionAnswerSerializer(lan, many=True)

        return Response({'data': serializer.data,
                         "s": s.data,
                         'result_a':result_a})


# url(r'^Questiondetails/?id=1$', views.Question_answer_details.as_view()),
class Question_details(APIView):
    """
    详情页面展示
    """
    def get(self,request):
        i = request.query_params.get('id')
        # print(query)
        data = LanguageCategory.objects.all()
        serializer = LanguageCategorySerializer(data, many=True)
        QuestionAnswer_data = Question.objects.filter(id=i)
        QuestionAnswer_dict = QuestionAnswerSerializer(QuestionAnswer_data,many=True)
        biaoqian = QuestionAnswer_dict.data[0]['label']['name']
        aa = Question.objects.filter(label__name=biaoqian).exclude(id=i)
        aa_list = QuestionAnswerSerializer(aa, many=True)
        aa_dict = aa_list.data
        return Response({'data':QuestionAnswer_dict.data[0],
                         'aa_list':aa_dict,
                         's':serializer.data,
                         })


class Usefulness(APIView):
    """问答详情页问题有用上下点击"""
    def get(self,request):
        # 获取问题id
        Question_id = request.query_params.get('id')
        # 获取点击用户的id
        user_id = request.query_params.get('user_id')
        # 获取点击是添加还是删除
        action = request.query_params.get('action')
        try:
            user_id = User.objects.filter(id=user_id)
        except Exception:
            # 验证失败，用户未登录
            return Response({'message':'404'})
        question = Question.objects.get(id=Question_id)
        if action == 'add':
            question.usefuls += 1
        else:
            if question.usefuls == 0:
                return Response({'message': '404'})
            else:
                question.usefuls -= 1
        question.save()
        return Response({'message':'ok'})


class AnswerUseful(APIView):
    """问答详情页回答有用上下点击"""
    def get(self,request):
        # 获取回答id
        answer_id = request.query_params.get('id')
        # 获取点击用户的id
        user_id = request.query_params.get('user_id')
        # 获取点击是添加还是删除
        action = request.query_params.get('action')
        try:
            user_id = User.objects.filter(id=user_id)
        except Exception:
            # 验证失败，用户未登录
            return Response({'message':404})
        answer = Answer.objects.get(id=answer_id)
        if action == 'add':
            answer.answer_usefuls += 1
        else:
            if answer.answer_usefuls == 0:
                return Response({'message': '404'})
            else:
                answer.answer_usefuls -= 1
        answer.save()
        return Response({'message':'ok'})


class CustomTagView(APIView):
    """
    常用标签页的展示
    """
    def get(self,request):
        categroy = LanguageCategory.objects.all()
        s1 = LanguageCategorySerializer(categroy, many=True)
        return Response({
            "categroy": s1.data
        })


class SubmitView(CreateAPIView):
    """
    问题提交
    """

    serializer_class = QuestioncCreateSerializer

    # def create(self, request, *args, **kwargs):
    #     serializer = self.get_serializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     serializer.save()
    #     return Response(serializer.data)


class LanguageFollowView(APIView):
    """
    展示关注
    """
    def get(self,request):
        user_id = request.query_params.get("user_id")

        user = User.objects.get(id=user_id)
        language_category = user.languagecategory_set.all()

        language_dict = LanguageCategory.objects.all()
        language_list =[]
        for language in language_dict:
            if language in language_category:
                language.follow = True

            language_list.append(language)
        serializer = LanguageFollowSerializer(language_list,many=True)
        return Response({'data':serializer.data})




class QueryTagViewView(APIView):
    """
    根据语言id查询是否被关注
    """
    def post(self,request):
        uid = request.data.get("uid")
        lid = request.data.get("lid")
        user = User.objects.get(id=uid)
        language_category = user.languagecategory_set.all()
        # 判断用户有没有关注语言
        if not language_category:
            return Response({'massage':'没关注'})

        # 查询出当前语言的对象
        language = LanguageCategory.objects.get(id=lid)

        # 判断当前的语言对象在不在用户关注的语言里面
        if language in language_category:
            return Response({'massage':'已关注'})
        else:
            return Response({'massage': '没关注'})




class AnswerCreateView(CreateAPIView):
    """
    提交回答
    """
    serializer_class = AnswerCreateSerializer

    def create(self, request, *args, **kwargs):
        print(request.data)
        # user_id = request.data.get('user')
        question_id = request.data.get('question')
        question = Question.objects.get(id=question_id)
        print(question.answers)
        question.answers += 1
        question.save()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


class AddFollowView(CreateAPIView):
    """
    点击添加关注
    """
    serializer_class = AddFollowCreateSerializer

    def post(self, request):
        user_id = request.data.get('uid')
        language_id = request.data.get('lid')
        user_id = User.objects.get(id=user_id)

        language_category = LanguageCategory.objects.get(id=language_id)
        language_category.attention +=1
        language_category.save()
        language_category.user.add(user_id)

        return Response({'massage':'ok'})


class DelFollowView(APIView):
    """
    点击取消关注
    """
    serializer_class = DelFollowCreateSerializer

    def post(self,request):
        user_id = request.data.get('uid')
        language_id = request.data.get('lid')
        user_id = User.objects.get(id=user_id)

        language_category = LanguageCategory.objects.get(id=language_id)

        language_category.attention -= 1
        language_category.save()

        language_category.user.remove(user_id)
        return Response({'massage': 'ok'})









