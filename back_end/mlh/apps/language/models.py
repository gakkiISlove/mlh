from django.db import models

# Create your models here.
# from utils.models import BaseModel
from mlh.utils.models import BaseModel
from users.models import User


class LanguageCategory(models.Model):
    """
    语言内容类别
    """
    WHETHER_OR_NOT_FOLLOW = (
        (0, "没关注"),
        (1, "已关注"),
    )

    user = models.ManyToManyField(User, verbose_name='用户', default=1)
    name = models.CharField(max_length=50, verbose_name='名称')
    attention = models.IntegerField(default=0, null=True, verbose_name='关注量')
    details = models.TextField(null=True, blank=True, verbose_name='标签介绍')
    encyclopedias = models.CharField(max_length=100, null=True, verbose_name='百科')
    follow = models.BooleanField(choices=WHETHER_OR_NOT_FOLLOW, default=0, verbose_name='是否关注')


    class Meta:
        db_table = 'tb_language_category'
        verbose_name = '语言类别'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class User2LanguageCategory(models.Model):

    uid = models.ForeignKey(User, to_field="id")
    lid = models.ForeignKey(LanguageCategory, to_field="id")


    class Meta:
        unique_together = ("uid", "lid")


class Label(models.Model):
    """
    标签
    """
    language_category = models.ForeignKey(LanguageCategory, on_delete=models.PROTECT, verbose_name='语言类别',null=True)
    name = models.CharField(max_length=50,null=True, verbose_name='名称')

    class Meta:
        db_table = 'tb_label'
        verbose_name = '标签内容'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class Question(BaseModel):
    """
    问题内容
    """
    STATE_OF_SOLVING = (
        (0, "没解决"),
        (1, "已解决"),
    )
    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='作者',default=1)
    language_category = models.ForeignKey(LanguageCategory, on_delete=models.PROTECT, verbose_name='类别')
    title = models.CharField(max_length=100, verbose_name='标题')
    content = models.TextField(null=True, blank=True, verbose_name='内容')
    usefuls = models.IntegerField(default=0,verbose_name='有用数量')
    answers = models.IntegerField(default=0,verbose_name='回答数量')
    browses = models.IntegerField(default=0,verbose_name='浏览量')
    solve = models.BooleanField(choices=STATE_OF_SOLVING, default=0, verbose_name='是否解决')
    label = models.ForeignKey(Label, on_delete=models.PROTECT, verbose_name='标签',default=1)

    class Meta:
        db_table = 'tb_question_answer'
        verbose_name = '问题内容'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.title


class Answer(BaseModel):
    """
    回答
    """
    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='作者', default=1)
    question = models.ForeignKey(Question, on_delete=models.PROTECT, verbose_name='回答的问题')
    answer_content = models.TextField(null=True, blank=True, verbose_name='内容')
    answer_usefuls = models.IntegerField(default=0, verbose_name='有用数量')

    class Meta:
        db_table = 'tb_answer'
        verbose_name = '回答表内容'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.answer_content

















