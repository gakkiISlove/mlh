from django.contrib import admin
from . import models


admin.site.register(models.User)
admin.site.register(models.Acount)
admin.site.register(models.UserArchives)
admin.site.register(models.JobExperience)
admin.site.register(models.EducationExperience)
admin.site.register(models.UserTeachnical)


