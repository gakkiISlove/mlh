import re

from django_redis import get_redis_connection
from rest_framework import serializers
from rest_framework.templatetags.rest_framework import data
from rest_framework_jwt.settings import api_settings

# from q_and_a.models import Question, Answer11
from language.models import LanguageCategory, Question, Answer
from users.models import User, UserArchives, JobExperience, EducationExperience, UserTeachnical, Area, Company


class CreateUserSerializer(serializers.ModelSerializer):
    """
    创建用户序列化器
    """
    sms_code = serializers.CharField(label='短信验证码', write_only=True)
    allow = serializers.CharField(label='同意协议', write_only=True)
    token = serializers.CharField(label='JWT token',read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'password', "my_avatar", 'sms_code', 'mobile', 'allow', 'token')
        extra_kwargs = {
            'username': {
                'min_length': 5,
                'max_length': 20,

                'error_messages': {
                    'min_length': '仅允许5-20个字符的用户名',
                    'max_length': '仅允许5-20个字符的用户名',
                }

            },
            'password': {
                'write_only': True,
                'min_length': 8,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许8-20个字符的密码',
                    'max_length': '仅允许8-20个字符的密码',
                }
            }
        }

    # 验证手机号
    def validate_mobile(self, value):
        if not re.match(r'^1[3-9]\d{9}$', value):
            raise serializers.ValidationError('手机格式错误')
        return value

    # 检验用户是否同意协议
    def validate_allow(self, value):
        if value != 'true':
            raise serializers.ValidationError('没有勾选同意协议')
        return value

    # 判断两次密码
    def validate(self, data):
        # 判断短信验证码
        redis_conn = get_redis_connection('verify_codes')
        mobile = data['mobile']
        real_sms_code = redis_conn.get('sms_%s' % mobile)
        # 在五分钟内需要输入验证码,否则验证码过期
        if real_sms_code is None:
            raise serializers.ValidationError('验证码无效')
        # 前端
        if real_sms_code.decode() != data['sms_code']:
            raise serializers.ValidationError('两次验证码输入不一致')

        return data

    # 创建用户(继承ModelSerializer里面有create方法，因为password要加密需要重写这个方法)
    def create(self, validated_data):
        # 删除数据库无效数据
        del validated_data['sms_code']
        del validated_data['allow']
        # 序列化器模型对象保存方式
        user = super().create(validated_data)
        # 这个是数据库的正常的保存方式
        # user=user.objects.create(**validated_data)
        # 调用django的认证系统加密
        user.set_password(validated_data['password'])
        user.save()
        # 签发jwt
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER # 加密处理
        # # 在token里面填充哪个用户信息
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        # # 之前的user里面没有token值，需要自己添加一个token，传到前面token字段里面
        user.token = token
        # 返回是序列化对象的序列化结果
        return user


class AreaSerializer(serializers.ModelSerializer):
    """行政区序列化"""

    class Meta:
        model = Area
        fields = ("id", "name")


class SubAreaSerializer(serializers.ModelSerializer):
    """子行政区序列化"""
    sub = AreaSerializer(many=True, read_only=True)

    class Meta:
        model = Area
        fields = ("id", "name", "subs")


class JobExperienceSeroializer(serializers.ModelSerializer):
    """工作经验序列化器"""

    class Meta:
        model = JobExperience
        fields = '__all__'


class EducationExperienceSeroializer(serializers.ModelSerializer):
    """教育经历序列化器"""

    class Meta:
        model = EducationExperience
        fields = '__all__'


class UserTeachnicalSeroializer(serializers.ModelSerializer):
    """用户技能序列化器"""

    class Meta:
        model = UserTeachnical
        fields = '__all__'


class UserHomeSerializer(serializers.ModelSerializer):
    """
    用户首页序列化器
    """
    user = CreateUserSerializer()
    jobexperience_set = JobExperienceSeroializer(many=True)
    educationexperience_set = EducationExperienceSeroializer(many=True)
    userteachnical_set = UserTeachnicalSeroializer(many=True)

    class Meta:
        model = UserArchives
        fields = ("id", "gender", "birth_date", "my_web_url", "email", "address", "user_introduction", "university",
                  "organization", "avatar_url", "complete_degree", "jobexperience_set", "educationexperience_set",
                  "userteachnical_set", "user", "city", "real_name", "phone")

    def update(self, instance, validated_data):
        change = validated_data['change']
        instance.change = change
        instance = super().update(instance, validated_data)
        return instance


class UserArchivesSerializer(serializers.ModelSerializer):
    """
    用户首页序列化器
    """

    class Meta:
        model = UserArchives
        fields = ("id", "gender", "birth_date", "my_web_url", "email", "address", "user_introduction", "university",
                  "organization", "avatar_url", "complete_degree", "city", "real_name", "phone")


class UserSerializer(serializers.ModelSerializer):
    """
    用户首页序列化器
    """
    # userarchives_set = UserArchivesSerializer()

    class Meta:
        model = User
        fields = '__all__'


class QuestionSerializer(serializers.ModelSerializer):
    """问题序列化器"""

    class Meta:
        model = Question
        fields = "__all__"


class AnswerSerializer(serializers.ModelSerializer):
    """回答序列化器"""
    class Meta:
        model = Answer
        fields = '__all__'


class CompanySerializer(serializers.ModelSerializer):
    """公司序列化器"""

    class Meta:
        model = Company
        fields = '__all__'


class UserAlterSerializer(serializers.ModelSerializer):
    """用户信息修改"""
    class Meta:
        model = UserArchives
        fields = "__all__"
