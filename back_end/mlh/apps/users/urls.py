from django.conf.urls import url
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token
from users.views import MyHomeViewSet
from . import views

urlpatterns = [

    url(r'^users/$', views.UserView.as_view()),
    url(r'^archives/$', views.UserArchivesView.as_view()),
    url(r'^useranswers/$', views.UserAnswerView.as_view()),
    url(r'^userquestions/$', views.UserQuestionView.as_view()),
    url(r'^questionshare/$', views.UserQuestionShareView.as_view()),
    url(r'^collectionspits/$', views.UserCollectionSpitView.as_view()),
    url(r'^collectionnews/$', views.UserCollectionNewsView.as_view()),
    url(r'^collectioncompanys/$', views.UserCollectionCompanyView.as_view()),
    url(r'^userfollowlabels/$', views.UserFollowLabelView.as_view()),
    url(r'^userfollowquestions/$', views.UserFollowQuestionView.as_view()),
    url(r'^usernames/(?P<username>\w{5,20})/count/$', views.UsernameCountView.as_view()),
    url(r'^mobiles/(?P<mobile>1[3-9]\d{9})/count/$', views.MobileCountView.as_view()),
    url(r'^authorizations/$', obtain_jwt_token),
    url(r'^useralter/$', views.UserAlterView.as_view()),
]

router = routers.DefaultRouter()
router.register(r'homes', MyHomeViewSet, base_name='homes')


urlpatterns += router.urls