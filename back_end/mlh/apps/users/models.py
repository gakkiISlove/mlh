from django.contrib.auth.models import AbstractUser
from django.db import models

from mlh.utils.models import BaseModel


class User(AbstractUser):
    """ 用户模型类"""
    mobile = models.CharField(max_length=11, unique=True, verbose_name='手机号')
    my_avatar = models.ImageField(null=True, verbose_name="个人小头像")

    class Meta:
        db_table = 'tb_users'
        verbose_name = '用户'
        verbose_name_plural = verbose_name


class Acount(BaseModel):
    """第三方账号绑定"""

    ACOUNT = (
        (0, "WECHART"), (1, "QQ"), (2, "SINA")
    )

    account = models.SmallIntegerField(choices=ACOUNT, default=0, null=True, verbose_name="性别")
    openid = models.CharField(max_length=64, null=True, verbose_name="第三方账号")
    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='用户')
    is_show = models.BooleanField(default=True, verbose_name="是否展示")
    is_bind = models.BooleanField(default=False, verbose_name="是否绑定")

    class Meta:
        db_table = 'tb_acount'
        verbose_name = '第三方账号'
        verbose_name_plural = verbose_name



class Area(models.Model):
    """
    行政区划
    """
    name = models.CharField(max_length=20, verbose_name='名称')
    parent = models.ForeignKey('self', on_delete=models.SET_NULL, related_name='subs', null=True, blank=True, verbose_name='上级行政区划')

    class Meta:
        db_table = 'tb_areas'
        verbose_name = '行政区划'
        verbose_name_plural = '行政区划'

    def __str__(self):
        return self.name


class UserArchives(BaseModel):

    GENDER = (
        (0, "MAN"), (1, "WOMAN")
    )

    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='用户')
    gender = models.SmallIntegerField(choices=GENDER, default=0, null=True, verbose_name="性别")
    birth_date = models.CharField(max_length=50, null=True, verbose_name="出生日期")
    city = models.CharField(max_length=50, null=True, verbose_name="居住城市")
    my_web_url = models.CharField(max_length=200, null=True, verbose_name="个人网站")
    email = models.CharField(max_length=100, null=True, verbose_name="邮箱")
    address = models.CharField(max_length=200, null=True, verbose_name="通讯地址")
    user_introduction = models.TextField(null=True, verbose_name="个人简介")
    university = models.CharField(max_length=50, null=True, verbose_name="毕业院校")
    organization = models.CharField(max_length=50, null=True, verbose_name="公司组织")
    avatar_url = models.ImageField(null=True, verbose_name="个人头像")
    complete_degree = models.FloatField(null=True, verbose_name="完善进度")
    real_name = models.CharField(max_length=50, null=True, verbose_name="真实姓名")
    phone = models.CharField(max_length=11, unique=True, verbose_name='手机号')

    class Meta:
        db_table = 'tb_user_archives'
        verbose_name = '用户档案'
        verbose_name_plural = verbose_name

    def __str__(self):
        return "用户档案"


class JobExperience(BaseModel):
    """工作经历表"""
    company = models.CharField(max_length=64, null=True, verbose_name='公司')
    position = models.CharField(max_length=64, null=True, verbose_name='职位')
    user_archives = models.ForeignKey(UserArchives, on_delete=models.PROTECT, null=True, verbose_name='用户档案')
    work_start_time = models.CharField(max_length=64, null=True, verbose_name='工作开始时间')
    work_end_time = models.CharField(max_length=64, null=True, verbose_name='工作结束时间')

    class Meta:
        db_table = 'tb_job_experience'
        verbose_name = '工作经历'
        verbose_name_plural = verbose_name

    def __str__(self):
        return '工作经历'


class EducationExperience(BaseModel):
    """教育经历表"""
    school = models.CharField(max_length=64, null=True, verbose_name='学校')
    education_background = models.CharField(max_length=64, null=True, verbose_name='学历')
    user_archives = models.ForeignKey(UserArchives, on_delete=models.PROTECT, null=True, verbose_name='用户档案')
    learn_start_time = models.CharField(max_length=64, null=True, verbose_name='学习开始时间')
    learn_end_time = models.CharField(max_length=64, null=True, verbose_name='学习结束时间')

    class Meta:
        db_table = 'tb_education_experience'
        verbose_name = '教育经历'
        verbose_name_plural = verbose_name

    def __str__(self):
        return '教育经历'


class UserTeachnical(BaseModel):
    """擅长技术表"""
    technical_name = models.CharField(max_length=64, null=True, verbose_name='技术名')
    user_archives = models.ForeignKey(UserArchives, on_delete=models.PROTECT, null=True, verbose_name='用户档案')

    class Meta:
        db_table = 'tb_technical'
        verbose_name = '擅长技术'
        verbose_name_plural = verbose_name

    def __str__(self):
        return '擅长技术'


class Company(models.Model):
    """公司表"""

    user = models.ManyToManyField(User, verbose_name='用户', default=1)
    name = models.CharField(max_length=100, verbose_name='公司名称')
    company_introduction = models.TextField(null=True, verbose_name="公司简介")
    collection_count = models.IntegerField(default=0, verbose_name='有用数量')
    job_count = models.IntegerField(default=0, verbose_name='招聘数量')

    class Meta:
        db_table = 'tb_company'
        verbose_name = '公司'
        verbose_name_plural = verbose_name


class UserCollectionCompany(models.Model):
    """用户收藏公司表"""
    user = models.ForeignKey(User, to_field="id")
    company = models.ForeignKey(Company, to_field="id")

    class Meta:
        unique_together = ("user", "company")


from news.models import NewsModel


class UserCollectionNews(models.Model):
    """用户收藏新闻表"""
    user = models.ForeignKey(User, to_field="id")
    news = models.ForeignKey(NewsModel, to_field="id")

    class Meta:
        unique_together = ("user", "news")


from tucao.models import Spit


class UserCollectionSpit(models.Model):
    """用户收藏吐槽表"""
    user = models.ForeignKey(User, to_field="id")
    spits = models.ForeignKey(Spit, to_field="id")

    class Meta:
        unique_together = ("user", "spits")


from language.models import Question


class UserFollowQuestion(models.Model):
    """用户收藏吐槽表"""
    user = models.ForeignKey(User, to_field="id")
    questions = models.ForeignKey(Question, to_field="id")

    class Meta:
        unique_together = ("user", "questions")