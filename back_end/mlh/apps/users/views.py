from rest_framework.generics import CreateAPIView, ListAPIView, GenericAPIView, RetrieveAPIView, UpdateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

# from q_and_a.models import Answer11, Question, QuestionCategory, User2QuestionCategory
# from q_and_a.serializers import QuestionCategorySerializer
from language.models import Answer, Question, LanguageCategory
from language.serializers import QuestionAnswerSerializer, LanguageCategorySerializer

from news.serializers import NewsListSerializer

from tucao.serializers import SpitSerializer
from users import models
from users.models import User, UserArchives, UserCollectionSpit, UserCollectionNews, Company, UserCollectionCompany, \
    UserFollowQuestion
from users import serializers
# from users.serializers import UserHomeSerializer, UserSerializer, UserArchivesSerializer
# from users.serializers import UserArchivesSerializer, UserHomeSerializer, QuestionSerializer, AnswerSerializer
from users.serializers import UserArchivesSerializer, UserHomeSerializer, AnswerSerializer, CompanySerializer, \
    UserAlterSerializer, CreateUserSerializer


class UserView(CreateAPIView):
    """用户注册"""
    serializer_class = serializers.CreateUserSerializer


class UsernameCountView(APIView):
    """用户数量，验证用户名"""

    def get(self, request, username):
        """
        获取指定用户名数量
        :param request:
        :param username:
        :return:
        """
        count = User.objects.filter(username=username).count()

        data = {
            'username': username,
            'count': count
        }
        return Response(data)


# 判断手机号是否存在
class MobileCountView(APIView):
    """验证手机号"""

    def get(self, request, mobile):
        count = User.objects.filter(mobile=mobile).count()

        data = {
            'mobile': mobile,
            'count': count
        }

        return Response(data)


class MyHomeViewSet(ModelViewSet):
    """我的主页"""

    queryset = UserArchives.objects.all()
    serializer_class = UserHomeSerializer
    permissions = [IsAuthenticated]


class UserArchivesView(RetrieveAPIView):
    """获取用户档案"""
    serializer_class = UserArchivesSerializer
    permissions = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        user_id = request.user.id
        user_archives = UserArchives.objects.filter(user_id=user_id).first()
        serializer = self.get_serializer(user_archives)
        return Response(serializer.data)


class UserAnswerView(ListAPIView):
    serializer_class = AnswerSerializer
    queryset = Answer.objects.all()
    permissions = [IsAuthenticated]

    def get_queryset(self):
        user_id = self.request.user.id
        return Answer.objects.filter(user_id=user_id)

    def list(self, request, *args, **kwargs):
        answers = self.get_queryset()

        for answer in answers:
            crate_time = answer.create_time
            update_time = answer.update_time
            crate_time = crate_time.strftime('%Y-%m-%d %H:%M:%S')
            update_time = update_time.strftime('%Y-%m-%d %H:%M:%S')
            answer.create_time = crate_time
            answer.update_time = update_time
        # page = self.paginate_queryset(questions)
        serializer = self.get_serializer(answers, many=True)
        return Response(serializer.data)


class UserQuestionView(ListAPIView):
    serializer_class = QuestionAnswerSerializer
    queryset = Question.objects.all()
    permissions = [IsAuthenticated]

    def get_queryset(self):
        user_id = self.request.user.id
        print(user_id)
        return Question.objects.filter(user_id=user_id)

    def list(self, request, *args, **kwargs):
        questions = self.get_queryset()

        for question in questions:
            crate_time = question.create_time
            update_time = question.update_time
            crate_time = crate_time.strftime('%Y-%m-%d %H:%M:%S')
            update_time = update_time.strftime('%Y-%m-%d %H:%M:%S')
            question.create_time = crate_time
            question.update_time = update_time
        # page = self.paginate_queryset(questions)
        serializer = self.get_serializer(questions, many=True)

        return Response(serializer.data)


class UserQuestionShareView(ListAPIView):
    serializer_class = QuestionAnswerSerializer
    queryset = Question.objects.all()
    permissions = [IsAuthenticated]

    def get_queryset(self):
        user_id = self.request.user.id
        print(user_id)
        return Question.objects.filter(solve=1)

    def list(self, request, *args, **kwargs):
        questions = self.get_queryset()

        for question in questions:
            crate_time = question.create_time
            update_time = question.update_time
            crate_time = crate_time.strftime('%Y-%m-%d %H:%M:%S')
            update_time = update_time.strftime('%Y-%m-%d %H:%M:%S')
            question.create_time = crate_time
            question.update_time = update_time
        # page = self.paginate_queryset(questions)
        serializer = self.get_serializer(questions, many=True)
        print(serializers.data)
        return Response(serializer.data)


class UserFollowLabelView(ListAPIView):
    """用户关注的标签"""

    serializer_class = LanguageCategorySerializer
    queryset = LanguageCategory.objects.all()
    permissions = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        user = User.objects.get(id=1)
        print(user)
        query_set = LanguageCategory.objects.filter(user=user)
        return query_set

    def list(self, request, *args, **kwargs):
        labels = self.get_queryset()
        serializer = self.get_serializer(labels, many=True)
        return Response(serializer.data)


class UserFollowQuestionView(ListAPIView):
    """用户关注的标签"""
    serializer_class = QuestionAnswerSerializer
    queryset = Question.objects.all()
    permissions = [IsAuthenticated]

    def get_queryset(self):
        user_id = self.request.user.id
        questions = UserFollowQuestion.objects.filter(user_id=user_id)
        question_id_list = []
        for question in questions:
            question_id_list.append(question.id)
        return Question.objects.filter(id__in=question_id_list)

    def list(self, request, *args, **kwargs):
        questions = self.get_queryset()

        for question in questions:
            crate_time = question.create_time
            update_time = question.update_time
            crate_time = crate_time.strftime('%Y-%m-%d %H:%M:%S')
            update_time = update_time.strftime('%Y-%m-%d %H:%M:%S')
            question.create_time = crate_time
            question.update_time = update_time
        serializer = self.get_serializer(questions, many=True)
        return Response(serializer.data)


from tucao.models import Spit


class UserCollectionSpitView(ListAPIView):
    serializer_class = SpitSerializer
    queryset = Spit.objects.all()
    permissions = [IsAuthenticated]

    def get_queryset(self):
        user_id = self.request.user.id
        spits = UserCollectionSpit.objects.filter(user_id=1)
        spit_id_list = []
        for spit in spits:
            spit_id_list.append(spit.id)
        return Spit.objects.filter(id__in=spit_id_list)

    def list(self, request, *args, **kwargs):
        spits = self.get_queryset()

        for spit in spits:
            crate_time = spit.create_time
            crate_time = crate_time.strftime('%Y-%m-%d %H:%M:%S')
            spit.create_time = crate_time
        serializer = self.get_serializer(spits, many=True)
        return Response(serializer.data)


from news.models import NewsModel


class UserCollectionNewsView(ListAPIView):
    serializer_class = NewsListSerializer
    queryset = NewsModel.objects.all()
    permissions = [IsAuthenticated]

    def get_queryset(self):
        user_id = self.request.user.id
        news_list = UserCollectionNews.objects.filter(user_id=user_id)
        print(news_list)
        news_id_list = []
        for news in news_list:
            news_id_list.append(news.id)
        return NewsModel.objects.filter(id__in=news_id_list)

    def list(self, request, *args, **kwargs):
        news_list = self.get_queryset()

        for news in news_list:
            crate_time = news.create_time
            crate_time = crate_time.strftime('%Y-%m-%d %H:%M:%S')
            news.create_time = crate_time
        serializer = self.get_serializer(news_list, many=True)
        return Response(serializer.data)


class UserCollectionCompanyView(ListAPIView):
    serializer_class = CompanySerializer
    queryset = Company.objects.all()
    permissions = [IsAuthenticated]

    def get_queryset(self):
        user_id = self.request.user.id
        companys = UserCollectionCompany.objects.filter(user_id=user_id)
        company_id_list = []
        for company in companys:
            company_id_list.append(company.id)
        return Company.objects.filter(id__in=company_id_list)


class UserAlterView(UpdateAPIView):

    serializer_class = UserAlterSerializer
    # queryset = User.objects.all().exists()
    permissions = [IsAuthenticated]

    def get_object(self):

        user_id = self.request.user.id
        archives = UserArchives.objects.get(user_id=user_id)
        if archives:
            return archives
        else:
            return UserArchives.objects.create(user=self.request.user)












