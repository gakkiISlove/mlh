let app = new Vue({
    el:"#app",
    data:{
        // 页面中需要使用到的数据，键值对
        data:[],
        s:[],
        num : 1,
        ban:1,
    },
    computed:{
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted:function () {
        // 一加载就需要做的，直接是代码


        axios.get(host+'/language/')

        .then(response=>{
            console.info(response.data)
            this.data = response.data['data']
            this.s = response.data['s']

        })
        .catch(error =>{
            alert('出错了')
        })
    },
    methods:{
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
        Banner: function (value) {
            axios.get(host+'/language?id='+value+'&new='+this.num,{
                responseType: 'json',
            })
            .then(response=>{
                console.info(response.data)
                this.s = response.data['s']
                this.ban = value
                this.num = this.num

            })
            .catch(error =>{
                alert('出错了111')
            })
        },
        list:function (value) {

            axios.get(host+'/sorting?new='+value+'&id='+this.ban,{
                responseType: 'json',
            })
            .then(response=>{
                console.info(response.data)
                this.s = response.data['s']
                this.num = value
            })
            .catch(error =>{
                alert('出错了')
            })
        }
    }

});
