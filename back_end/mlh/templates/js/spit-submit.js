var vm = new Vue({
    el:'#spit_app',
    data:{
        host: host,
        message: '请输入吐槽内容',
        user_id: '',
        spit_url: '',
        spit_comments: [],
    },
    mounted:function () {
        this.spit_url = this.get_query_string('user_id');
    },
    methods: {
        // 获取url路径参数
        get_query_string: function(name){
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        },
        send_spit: function () {
            axios.post(this.host + '/spit-submit/',{
                content: this.message,
                user_id: '1'
            } ,{
            responseType:'json'
        }).then(response => {
            window.location.href="http://127.0.0.1:8080/spit-index.html"
        })
        .catch(error => {
            console.log(error.response.data)
        });
        }
    }
})