var host = 'http://127.0.0.1:8000';

// str转换成时间对象
// 时间对象转换成ｙｍｄ
// 时间对象转换成ｙｍｄｈｍ
// 时间对象转换成周几

function Appendzero(obj)
    {
        if(obj<10) return "0" +""+ obj;
        else return obj;
    }



// str转换成时间对象
Vue.filter('str_datetime', function (str) {
  return new Date(str)
});

// 时间对象转换成:xx年xx月xx日
Vue.filter('date_ymd', function (date) {
  return date.getFullYear() + '-' + Appendzero(date.getMonth() + 1) + "-" + Appendzero(date.getDate())
});

// 时间对象转换成：　xx(分)：xx(秒)
Vue.filter('date_hms', function (date) {
  return Appendzero(date.getHours()) + ":" + Appendzero(date.getMinutes())
});

// 时间对象转换成:  周x
const WEEK = ["周日", "周一", "周二", "周三", "周四", "周五", "周六"]
Vue.filter('date_week', function (date) {
  return WEEK[date.getDay()]
});


// 时间对象转换成: xx月xx日 时:分
Vue.filter('date_md_hm', function (date) {
  return (date.getMonth() + 1) + "月" + date.getDate() + '日' + ' ' + date.getHours() + ':' + date.getMinutes()
});
