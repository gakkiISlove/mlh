from rest_framework import serializers
from .models import Activity


class ActivityIndexSerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = ('id' ,'title', 'time', 'city', 'status', 'image_url')

class ActivityDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity