from rest_framework import serializers

from .models import Spit, SpitComment
from users.models import User


class SpitSerializer(serializers.ModelSerializer):
    """吐槽列表序列化器"""
    class Meta:
        model = Spit
        fields = ('id', 'content', 'create_time', 'like_count', 'user_id')


class SpitCommentSeriazer(serializers.ModelSerializer):
    """吐槽评论序列器"""


    class Meta:
        model = SpitComment
        fields = ('id', 'content', 'like_count', 'create_time', 'parent_id')