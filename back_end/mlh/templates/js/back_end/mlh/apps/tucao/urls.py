from django.conf.urls import url
from rest_framework.routers import DefaultRouter

from . import views

urlpatterns = [
    url(r'^spit-index/$', views.SpitList.as_view()),
    url(r'^spit-detail/$', views.SpitDetail.as_view())
]

router = DefaultRouter()
router.register('spit_comment', views.Spit_Comment, base_name='spit_comment')

urlpatterns += router.urls