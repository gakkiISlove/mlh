import random
from celery_tasks.sms import tasks as sms_tasks
from django.shortcuts import render

# Create your views here.
from django_redis import get_redis_connection
from rest_framework.response import Response
from rest_framework.views import APIView

from mlh.apps.verifications import constants


class SMSCodeView(APIView):
    def get(self, mobile):
        """创建短信验证码"""
        sms_code = '%06d' % random.randint(0, 999999)

        # 保存短信验证码与发送验证码
        redis_conn = get_redis_connection('verify')
        pl = redis_conn.pipeline()
        pl.setex("sms_%s" % mobile, constants.SMS_CODE_REDIS_EXPIRES, sms_code)
        pl.setex("send_flag_%s" % mobile, constants.SEND_SMS_CODE_INTERVAL, 1)
        pl.execute()

        # 发送短信验证码
        sms_code_expires = str(constants.SMS_CODE_REDIS_EXPIRES // 60)
        sms_tasks.send_sms_code.delay(mobile, sms_code, sms_code_expires)

        return Response({"message": "OK"})
