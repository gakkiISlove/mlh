from django.db import models

# Create your models here.
from mlh.utils.models import BaseModel


class SectionModel(BaseModel):
    """项目版块"""
    name = models.CharField(max_length=16, verbose_name='名称')
    sort = models.IntegerField(verbose_name='排序')

    class Meta:
        db_table = 'tb_section'
        verbose_name = '项目版块'


class ColumnModel(BaseModel):
    """头条专栏"""
    COLUMN_STATUS = (
        (-1, '未通过'),
        (0, '待审核'),
        (1, '已通过')
    )

    name = models.CharField(max_length=16, verbose_name='名称')
    sort = models.IntegerField(verbose_name='排序')
    url = models.CharField(max_length=64, verbose_name='网址')
    introduce = models.CharField(max_length=128, verbose_name='简介')
    status = models.SmallIntegerField(choices=COLUMN_STATUS, default=0, verbose_name='状态')


class NewsModel(BaseModel):
    """"头条新闻"""
    column_id = models.ForeignKey(ColumnModel, on_delete=models.PROTECT, verbose_name='专栏id')
    title = models.CharField(max_length=64, verbose_name='标题')
    author = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='作者')  # TODO user模型还没有
    content = models.TextField(verbose_name='新闻内容')
    image = models.ImageField(verbose_name='新闻图片')
    comment_count = models.IntegerField(verbose_name='评论总数')


class CommentModel(BaseModel):
    """新闻评论"""
    user_id = models.ForeignKey()  # TODO user模型还没有
    news_id = models.ForeignKey(NewsModel, on_delete=models.PROTECT, verbose_name='新闻id')
    parent_id = models.ForeignKey('self', on_delete=models.PROTECT, verbose_name='父评论id')
    content = models.CharField(max_length=128, verbose_name='评论内容')

