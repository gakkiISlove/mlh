/**
 * Created by python on 18-11-8.
 */
var vm_head = new Vue({
    el:"#activity_detail_head_app",
    data:{
        // 页面中需要使用到的数据，键值对
        host:host,
        user_id:sessionStorage.user_id || localStorage.user_id,
        user_name:sessionStorage.username || localStorage.username,
    },
    computed:{
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted:function () {
        // 一加载就需要做的，直接是代码
    },
    methods:{
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
    }
});


var vm = new Vue({
    el:"#activity_detail_app",
    data:{
        // 页面中需要使用到的数据，键值对
        host:host,
        activity:null,
        activity_id :null,
        now_mc:0,
        deadline_mc:0,
        last_mc:0,
        user_id:sessionStorage.user_id || localStorage.user_id,
        user_name:sessionStorage.username || localStorage.username,
        token:localStorage.token || sessionStorage.token,

    },
    computed:{
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted:function () {
        // 一加载就需要做的，直接是代码
        this.activity_id = this.get_query_string("id")
        axios.get(this.host+'/activities/' + this.activity_id + "/", {
                            headers: {
                                'Authorization': 'JWT ' + this.token,
                            },
                            withCredentials:true
                        })
            .then(response=>{
                this.activity = response.data
                let now = response.headers.date
                now = new Date(now)
                now = now.getTime()
                deadline = new Date(response.data.start_time).getTime()
                this.deadline_count(deadline, now)
            })
    },

    methods: {
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
        get_query_string: function (name, def_val) {
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return def_val;
        },
        deadline_count: function (deadline_mc, now_mc) {
            this.deadline_mc = deadline_mc
            this.now_mc = now_mc
            console.log(this.deadline_mc);
            console.log(this.now_mc);
            setInterval(()=> {
                this.now_mc += 1000
                this.last_mc = (this.deadline_mc - this.now_mc) / 1000
            }, 1000)
        },
        activity_apply:function () {

            axios.post(this.host+'/activities/' + this.activity_id + "/apply", {},{
                            headers: {
                                'Authorization': 'JWT ' + this.token
                            },
                            responseType: 'json'
                        })
            .then(response=>{

                if (response.status=="201"){
                    alert("报名成功")
                }
                else {

                }
            })
        }
    }
});