/**
 * Created by python on 18-11-8.
 */

var vm0 = new Vue({
    el:"#activity_head_app",
    data:{
        // 页面中需要使用到的数据，键值对
        host:host,
        user_id:sessionStorage.user_id || localStorage.user_id || 0,
        user_name:sessionStorage.username || localStorage.username,
    },
    computed:{
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted:function () {
        // 一加载就需要做的，直接是代码
    },
    methods:{
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
    }
});

var vm = new Vue({
    el:"#activity_app",
    data:{
        // 页面中需要使用到的数据，键值对
        host:host,
        activities:[],
    },
    computed:{
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted:function () {
        // 一加载就需要做的，直接是代码
        axios.get(this.host+'/activities/')
            .then(response=>{
                this.activities = response.data
            })
            .catch(error =>{

            })
    },
    methods:{
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数

    }
})
