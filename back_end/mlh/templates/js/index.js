var host = 'http://127.0.0.1:8000';

// str转换成时间对象
// 时间对象转换成ｙｍｄ
// 时间对象转换成ｙｍｄｈｍ
// 时间对象转换成周几

// str转换成时间对象
Vue.filter('str_datetime', function (str) {
  return new Date(str)
});

// 时间对象转换成:xx年xx月xx日
Vue.filter('date_ymd', function (date) {
  return date.getFullYear() + '-' + (date.getMonth() + 1) + "-" + date.getDate()
});

// 时间对象转换成：　xx(分)：xx(秒)
Vue.filter('date_hms', function (date) {
  return date.getHours() + ":" + date.getMinutes()
});

// 时间对象转换成:  周x
const WEEK = ["周日", "周一", "周二", "周三", "周四", "周五", "周六"]
Vue.filter('date_week', function (date) {
  return WEEK[date.getDay()]
});


// 时间对象转换成: xx月xx日 时:分
Vue.filter('date_md_hm', function (date) {
  return (date.getMonth() + 1) + "月" + date.getDate() + '日' + ' ' + date.getHours() + ':' + date.getMinutes()
});



var app = new Vue({
    el: "#app",
    data: {
        // 页面中需要使用到的数据，键值对
        host: host,
        sections: [],  // 版块
        columns: [],  // 专栏
        news_list: [],  // 新闻列表
        num_column: 0,  // 专栏点击赋予样式的变量
        num_section: 0,  // 版块点击切换样式的变量
        user_id:sessionStorage.user_id || localStorage.user_id,
        user_name:sessionStorage.username || localStorage.username,
        current_column_id: 1,
        show: true,
        box_show: 0,

    },
    computed: {
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted: function () {
        this.get_section();
        this.get_column();
        this.get_news_list();
    },
    methods: {
        // 首页.获取版块
        get_section: function () {
            axios.get(this.host + '/sections/')
                .then(response => {
                    this.sections = response.data;
                })
                .catch(error => {
                    alert('获取版块名出错!')
                });
        },
        // 首页.获取专栏
        get_column: function(){
            axios.get(this.host + '/columns/')
                .then(response => {
                    this.columns = response.data;
                })
                .catch(error => {
                    alert('专栏错误!')
                })
        },
        // 首页.获取新闻列表
        get_news_list: function(){
            axios.get(this.host + '/news_list' + '?column_id=' + this.current_column_id)
                .then(response => {
                    this.news_list = response.data;
                })
                .catch(error => {
                    alert('获取新闻列表失败!')
                })
        },
        // 首页.专栏点击，切换样式
        change_column_class: function(index, current_column_id){
            this.current_column_id = current_column_id;
            this.num_column=index;
            this.get_news_list();
        },
        // 首页.版块点击，切换样式
        change_section_class: function(index){
             this.num_section=index;
        },
        // 跳转到新闻详情页
        change_html: function(news_id){
            window.location.href = '/headline-detail.html?news_id=' + news_id
        },
        // 鼠标划入头像，出现发布栏
        show_box: function(num){
            this.box_show = num
        },
        // 鼠标划出头像，隐藏发布栏
        hide_box: function(num){
            this.box_show = num
        }　

    }
})






