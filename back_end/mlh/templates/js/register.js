var vm = new Vue({
    el: '#app',
    data: {
        host,  //host:host
        error_name: false,
        error_password: false,
        error_check_password: false,
        error_phone: false,
        error_allow: false,
        error_image_code: false,
        error_sms_code: false,
        error_name_message: '',
        error_image_code_message: '',
        error_phone_message: '',
        error_sms_code_message: '',
        sms_code_tip: '获取短信验证码',
        sending_flag: false, // 正在发送短信标志
        username: '',
        password: '',
        password2: '',
        mobile: '',
        sms_code: '',
        allow: false
    },

    mounted: function () {

    },

    methods: {
        // 检查用户名
        check_username: function () {
            var len = this.username.length;
            if (len < 5 || len > 20) {
                this.error_name = true;
            } else {
                this.error_name = false;
            }
            // 检查重名
            if (this.error_name == false) {
                axios.get(this.host + '/usernames/' + this.username + '/count/', {
                    responseType: 'json'
                })
                    .then(response => {
                        if (response.data.count > 0
                        ) {
                            this.error_name_message = '用户名已存在';
                            this.error_name = true;
                        }
                        else {
                            this.error_name = false;
                        }
                    })
                    .catch(error => {
                        console.log(error.response.data);
                    })
            }
        },

        // 检查手机号
        check_pwd: function () {
            var len = this.password.length;
            if (len < 8 || len > 20) {
                this.error_password = true;
            } else {
                this.error_password = false;
            }
            if (this.error_phone == false) {
                axios.get(this.host + '/mobiles/' + this.mobile + '/count/', {
                    responseType: 'json'
                })
                    .then(response => {
                        if (response.data.count > 0
                        ) {
                            this.error_phone_message = '手机号已存在';
                            this.error_phone = true;
                        }
                        else {
                            this.error_phone = false;
                        }
                    })
                    .catch(error => {
                        console.log(error.response.data);
                    })
            }
        },

        // 检查短信验证码
        check_sms_code: function () {
            if (!this.sms_code) {
                this.error_sms_code = true;
            } else {
                this.error_sms_code = false;
            }
        },

        // 检查是否勾选
        check_allow: function () {
            if (!this.allow) {
                this.error_allow = true;
            } else {
                this.error_allow = false;
            }
        },

        // 发送手机短信验证码
        send_sms_code: function () {

            if (this.sending_flag == true) {
                return;
            }
            this.sending_flag = true;
            // 校验参数，保证输入框有数据填写
            // this.check_phone();
            if (this.error_phone == true) {
                this.sending_flag = false;
                return;
            }

            axios.get(this.host + '/sms_codes/' + this.mobile, {
                // axios.get("http://127.0.0.1:8000/sms_codes/15527291123/",{
                // headers: "Access-Control-Allow-Origin: *",
                responseType: 'json',
            })
                .then(response => {
                    alert(1111)
                    var num = 60;
                    // 设置一个计时器
                    var t = setInterval(() => {
                            if (num == 1
                            ) {
                                // 如果计时器到最后, 清除计时器对象
                                clearInterval(t);
                                // 将点击获取验证码的按钮展示的文本回复成原始文本
                                this.sms_code_tip = '获取短信验证码';
                                // 将点击按钮的onclick事件函数恢复回去
                                this.sending_flag = false;
                            }
                            else {
                                num -= 1;
                                // 展示倒计时信息
                                this.sms_code_tip = num + '秒';
                            }
                        },
                        1000, 60
                    )
                })
                .catch(error => {
                    alert(2222)
                    console.log(error.response)
                    if (error.response.status == 400) {
                        this.error_image_code_message = '图片验证码有误';
                        this.error_image_code = true;
                    }
                    else {
                        console.log(error.response.data);
                    }
                    this.sending_flag = false;
                })
        },

        // 注册
        on_submit: function () {
            this.check_username();
            this.check_pwd();
            this.check_sms_code();
            this.check_allow();
            if (this.error_name == false && this.error_password == false
                && this.error_sms_code == false && this.error_allow == false) {
                axios.post(this.host + '/users/', {
                    username: this.username,
                    password: this.password,
                    mobile: this.mobile,
                    sms_code: this.sms_code,
                    allow: this.allow.toString()
                }, {
                    responseType: 'json'
                })
                .then(response => {
                    alert("33333")
                    // 记录用户的登录状态
                    sessionStorage.clear();
                    localStorage.clear();

                    // localStorage.token = response.data.token;
                    localStorage.username = response.data.username;
                    localStorage.user_id = response.data.id;
                    location.href = 'person-loginsign.html';
                })
                .catch(error => {
                    if (error.response.status == 400
                    ) {
                        this.error_sms_code_message = '数据有误';
                    }
                    else {
                        console.log(error.response.data);
                    }
                })
            }
        },


    },

});