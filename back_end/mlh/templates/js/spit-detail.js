var vm = new Vue({
    el:'#spit_app',
    data:{
        host: host,
        spit: {},
        isLike: false,
        notLike: true,
        spit_id: '',
        spit_url: '',
        spit_comments: [],
    },
    mounted:function () {
        // this.get_spit_id();
        this.spit_url = this.get_query_string('spit_id');
        axios.get(this.host + '/spit-detail/?spit_id=' + this.spit_url, {
            responseType:'json'
        }).then(response => {
            this.spit = response.data
        })
        .catch(error => {
            console.log(error.response.data)
        });
        this.get_spit_comment()
    },
    methods: {
        like: function () {
            if(this.isLike==true){
                this.isLike=false;
                this.notLike=true
            }else{
                this.notLike=false;
                this.isLike=true
            }
        },
        // get_spit_id: function(){
        //     var re = /^\/spit-detail\/(\d+).html$/;
        //     this.spit_id = document.location.pathname.match(re)[1];
        // },
        // 获取url路径参数
        get_query_string: function(name){
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        },
        get_spit_comment: function () {
            axios.get(this.host + '/spit_comment/?spit_id=' + this.spit_url, {
            responseType:'json'
        }).then(response => {
            this.spit_comments = response.data
        })
        .catch(error => {
            console.log(error.response.data)
        });
        }
    }
})