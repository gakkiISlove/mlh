var vm = new Vue({
    el:'#spit_app',
    data:{
        host: host,
        spits: [],
        spit_likes:[],
        spit_collections:[],
        action:""

    },
    mounted:function () {
        axios.get(this.host + '/spit-index/', {
            responseType:'json'
        }).then(response => {
            this.spits = response.data
        })
        .catch(error => {
            console.log(error.response.data)
        });
        this.get_spit_like();
        this.get_spit_collection()
    },
    methods: {
        like: function (spit_id) {
        axios.post(this.host + '/spit_like_action/',{
                user_id: '1',
                spit_id:spit_id,
                action:'add'
            } ,{
            responseType:'json'
        }).then(response => {
            window.location.reload()
        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
        notlike:function (spit_id) {
        axios.post(this.host + '/spit_like_action/',{
                user_id: '1',
                spit_id:spit_id,
                action:'cancel'
            } ,{
            responseType:'json'
        }).then(response => {
            window.location.reload()
        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
        collection: function (spit_id) {
            axios.post(this.host + '/spit_collection_action/',{
                user_id: '1',
                spit_id:spit_id,
                action:'add'
            } ,{
            responseType:'json'
        }).then(response => {
            window.location.reload()
        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
        cancelcollection: function (spit_id) {
        axios.post(this.host + '/spit_collection_action/',{
                user_id: '1',
                spit_id:spit_id,
                action:'cancel'
            } ,{
            responseType:'json'
        }).then(response => {
            window.location.reload()
        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
        get_spit_detail: function (spit_id) {
            window.location.href="http://127.0.0.1:8080/spit-detail.html?spit_id=" + spit_id;
        },
        get_spit_like: function () {
            axios.get(this.host + '/spit_like/?user_id=1', {
            responseType:'json'
        }).then(response => {
            this.spit_likes = response.data['spit_likes'];
            console.log(this.spit_likes)
        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
        get_spit_collection: function () {
        axios.get(this.host + '/spit_collection/?user_id=1', {
            responseType:'json'
        }).then(response => {
            this.spit_collections = response.data['spit_collections']
        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
    }
})