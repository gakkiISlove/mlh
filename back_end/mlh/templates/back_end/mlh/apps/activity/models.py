from django.db import models

from mlh.utils.models import BaseModel


class Activity(BaseModel):
    STATUS_ENUM = {
        "BEFORE":1,
        "SIGNING":2,
        "END":3
    }

    STATUS_ENUM_CHIOCES = (
        (1, "立即报名"),
        (2, "活动进行中"),
        (3, "活动结束")
    )

    title = models.CharField(verbose_name="活动标题", max_length=32)
    image_url = models.CharField(verbose_name="活动图片", max_length=32)
    time = models.DateField(verbose_name="时间")
    city = models.CharField(verbose_name="城市", max_length=16)
    address_detail = models.CharField(verbose_name="举办地点", max_length=32)
    digest = models.TextField(verbose_name="活动摘要")
    content = models.TextField(verbose_name="活动内容")
    start_time = models.DateTimeField(verbose_name="开始时间")
    stop_time = models.DateTimeField(verbose_name="结束时间")
    deadline = models.DateTimeField(verbose_name="报名截止")
    organizer = models.CharField(verbose_name="主办方", max_length=16)
    status = models.SmallIntegerField(choices=STATUS_ENUM_CHIOCES, default=1)
    is_delete = models.BooleanField(default=False)

    class Meta:
        db_table = "tb_activity"
        verbose_name = "活动"

