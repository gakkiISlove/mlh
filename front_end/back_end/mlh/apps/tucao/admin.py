from django.contrib import admin
from .models import Spit, SpitComment

admin.site.register(Spit)
admin.site.register(SpitComment)
# admin.site.register(HeroInfo)