import re
from django_redis import get_redis_connection
from rest_framework import serializers
from rest_framework.templatetags.rest_framework import data
from rest_framework_jwt.serializers import User


class CreateViewSerializer(serializers.ModelSerializer):
    """创建用户序列化器"""
    sms_code = serializers.CharField(label='短信验证码', write_only=True)
    allow = serializers.CharField(label='同意协议', write_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'sms_code', 'mobile', 'allow')
        extra_kwargs = {
            'username': {
                'min_length': 5,
                'max_length': 20,

                'error_messages': {
                    'min_length': '仅允许5-20个字符的用户名',
                    'max_length': '仅允许5-20个字符的用户名',
                }

            },
            'password': {
                'write_only': True,
                'min_length': 8,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许8-20个字符的密码',
                    'max_length': '仅允许8-20个字符的密码',
                }
            }
        }

    # 验证手机号
    def validate_mobile(self,value):
        if not re.match(r'^1[3-9]\d{9}$',value):
            raise serializers.ValidationError('手机号格式不对')
        return value

    # 同意协议
    def validate_allow(self,value):
        if value != True:
            raise serializers.ValidationError('没有勾选同意协议')

        # 判断短信验证码
        redis_conn=get_redis_connection('verify_codes')
        mobile = data['mobile']
        real_sms_code = redis_conn.get('sms_%s' % mobile)
        # 在五分钟内需要输入验证码,否则验证码过期
        if real_sms_code is None:
            raise serializers.ValidationError('验证码无效')
        # 前端
        if real_sms_code.decode() != data['sms_code']:
            raise serializers.ValidationError('两次验证码输入不一致')

        return data

    def create(self, validated_data):
        # 删除数据库无效数据
        del validated_data['sms_code']
        del validated_data['allow']
        # 序列化器模型对象保存方式
        user = super().create(validated_data)
        # 这个是数据库的正常的保存方式
        # user=user.objects.create(**validated_data)
        # 调用django的认证系统加密
        user.set_password(validated_data['password'])
        return user
        # user.save()
        # # 签发jwt
        # jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        # jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER  # 加密处理
        # # 在token里面填充哪个用户信息
        # payload = jwt_payload_handler(user)



