let apps = new Vue({
    el:"#apps",
    data:{
        // 页面中需要使用到的数据，键值对
        data:[],
        value:'',
        count:'',
        aa_list:[],
        s:[],
        num:1,
        ban:1,
        // user_id:1,
        action:'',
        txt:false,
        message:'',
        answer_id:'',
        index:'',
        a1:true,
        a2:true,
        user_id:sessionStorage.user_id || localStorage.user_id,
        user_name:sessionStorage.username || localStorage.username,

    },
    computed:{
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted:function () {
        // 一加载就需要做的，直接是代码
        // 展示问题页面
        this.value = this.get_query_string('id');
        axios.get(host+'/questiondetails/?id='+this.value, {
            responseType:'json'
        })
        .then(response=>{

            this.data = response.data['data']
            this.s = response.data['s']
            console.info(this.s)

            this.count = this.data['answers']
            this.aa_list = response.data['aa_list']


        })
        .catch(error =>{
            alert('出错了222')
        })
    },
    methods:{
        // 获取url路径参数
        get_query_string: function(name){
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        },
        // 点击语言展示
        Banner: function (value) {
            axios.get(host+'/language?id='+value+'&new='+this.num,{
                responseType: 'json',
            })
            .then(response=>{
                console.info(response.data)
                this.s = response.data['s']
                this.ban = value
                this.num = this.num
                window.location.href='./qa-logined.html?id='+this.ban+'&new='+this.num
            })
            .catch(error =>{
                alert('出错了111')
            })
        },

        // 点击问题有用添加删除
        Useful: function (action) {
            if (this.user_id>0){
                this.action=action
                axios.get(host+'/useful?id='+this.value+'&user_id='+this.user_id+'&action='+this.action,{
                    responseType: 'json',
                })
                .then(response=>{
                    console.info(response.data)
                    // window.location.reload()
                    if (response.status == 200){
                        if (this.action == 'add'){
                            this.data.usefuls +=1
                            this.a1 = false

                        }
                        else {
                            if (this.data.usefuls==0){
                                alert('不能在点了，已经没了')
                            }
                            else {
                                this.data.usefuls -=1
                                this.a1 = true
                            }
                        }
                    }
                    else {
                        alert('点击失败请从新')
                    }

                })
                .catch(error =>{
                    alert('出错了')
                })

            }
            else {
                    alert('请先登陆')
                }
        },

        huida:function () {
            if (this.user_id>0){
                this.txt=!this.txt
            }
            else {
                alert('请先登陆在回答')
                location.href = './person-loginsign.html'
            }

        },

        answer: function () {
            // 提交问题
             if (this.value&&this.message&&this.user_id) {
                 axios.post(host + '/answer/', {
                     // 通过axios将所获得的数据传送到后端
                     user: this.user_id,  //用户id
                     answer_content: this.message,    // 回答的内容
                     question: this.value    // 问题的id
                 }, {
                     responseType: 'json'
                 }).then(response => {
                     window.location.href = "./qa-detail.html?id=" + this.value
                 })
                     .catch(error => {
                         console.log(error.response.data)
                     });
             }else {
                 alert('请输入正确的数据')
             }
        },

        // 点击回答有用添加删除
        AnswerUseful: function (action,answer_id,index) {
            // 回答的有用无用
            // alert(index)
            if (this.user_id>0){
                this.action = action
                this.answer_id = answer_id
                this.index = index
                // alert(this.index)
                axios.get(host + '/answeruseful?id=' + this.answer_id + '&user_id=' + this.user_id + '&action=' + this.action, {
                    responseType: 'json',
                })
                    .then(response => {

                        console.info(response.data)
                        if (response.status == 200) {

                            if (this.action == 'add') {
                                this.data.answer_set[this.index].answer_usefuls += 1
                                this.a2 = false
                            }
                            else {
                                if (this.data.answer_set[this.index].answer_usefuls == 0) {
                                    alert('不能在点了，已经没了')
                                }
                                else {
                                    this.data.answer_set[this.index].answer_usefuls -= 1
                                    this.a2 = true
                                }
                            }
                        }
                        else {
                            alert('点击失败请从新')
                        }
                    })
                    .catch(error => {
                        alert('出错了')
                    })
            }
            else {
                 alert('请先登陆')
            }
        },
    }

});
