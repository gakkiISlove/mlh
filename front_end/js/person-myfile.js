var vm = new Vue({
    el: '#app',
    data: {
        host,  //host:host
        archives: null,
        gender: 0,
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        archive_id: 0,
        styleObject: {
            display: 'block',
            },

    },

    mounted: function () {
        this.get_archives();

    },

    methods: {
        // 退出
        logout(){
            sessionStorage.clear();
            localStorage.clear();
            location.href = '/login.html';
        },

        // 获取用户档案id
        get_archives: function () {
            axios.get(this.host + '/archives/', {

                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
            })
                .then(response => {
                    this.archive_id = response.data.id;
                    this.get_user_home();
                })
                .catch(error => {
                    alert(error.response.status);
                    console.log(error.response.status)
                })
        },


        // 获取用户档案信息
        get_user_home: function () {
            console.log(this.archive_id)
            axios.get(this.host + '/homes/' + this.archive_id +"/", {

                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
            })
            .then(response => {
                this.archives = response.data;
                this.gender = this.archives.gender
            })
            .catch(error => {
                alert(error.response.status);
                console.log(error.response.status)
            })
        },


        // 修改档案
        modify_archives: function () {
            this.styleObject={
                display: 'none',
                };
            axios.patch(this.host + '/useralter/',
                {
                    real_name : this.archives.real_name,
                    birth_date : this.archives.birth_date,
                    phone : this.archives.phone,
                    city : this.archives.city,
                    gender : this.archives.gender,
                    my_web_url : this.archives.my_web_url,
                    email : this.archives.email,
                    address : this.archives.address,
                },
                {
                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
            })
            .then(response => {
                this.archives = response.data;
                window.location.reload()
            })
            // .catch(error => {
            //     alert(error.response.status);
            //     console.log(error.response.status)
            // })

        },


        // 修改style属性
        modify_style: function () {
            this.styleObject={
                display: 'block',
                };
        },


    }

});