var apps = new Vue({
    el:"#apps",
    data:{
        // 页面中需要使用到的数据，键值对
        data:[],
        language_id:'',
        user_id:sessionStorage.user_id || localStorage.user_id,
        user_name:sessionStorage.username || localStorage.username,
    },
    computed:{
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted:function () {
        // 一加载就需要做的，直接是代码
        axios.get(host+'/follow/?user_id='+this.user_id ,{
            responseType:'json',
        })
        .then(response=>{
            this.data = response.data['data']
            console.info(this.data)
        })
        .catch(error =>{
            alert('出错了')
        })
    },
    methods:{
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
        addfollow:function (language_id) {
            //添加关注
            if (this.user_id){
                this.language_id = language_id
                axios.post(host + '/addfollow/', {
                     // 通过axios将所获得的数据传送到后端
                     uid: this.user_id,  //用户id
                     lid: this.language_id,    // 回答的内容
                 }, {
                     responseType: 'json'
                 }).then(response => {
                    location.reload()
                 }).catch(error => {
                    console.log(error.response.data)
                 });
            }else {
                alert('请先登陆')
            }
        }
    }
})



