// str转换成时间对象
// 时间对象转换成ｙｍｄ
// 时间对象转换成ｙｍｄｈｍ
// 时间对象转换成周几

// str转换成时间对象
Vue.filter('str_datetime', function (str) {
  return new Date(str)
});

// 时间对象转换成:xx-xx-xx
Vue.filter('date_ymd', function (date) {
  return date.getFullYear() + '-' + (date.getMonth() + 1) + "-" + date.getDate()
});

// 时间对象转换成：　xx(分)：xx(秒)
Vue.filter('date_hms', function (date) {
  return date.getHours() + ":" + date.getMinutes()
});

// 时间对象转换成:  周x
const WEEK = ["周日", "周一", "周二", "周三", "周四", "周五", "周六"]
Vue.filter('date_week', function (date) {
  return WEEK[date.getDay()]
});

// 时间对象转换成: xx月xx日 时:分
Vue.filter('date_md_hm', function (date) {
  return (date.getMonth() + 1) + "月" + date.getDate() + '日' + ' ' + date.getHours() + ':' + date.getMinutes()
});





var app = new Vue({
    el: "#app",
    data: {
        // 页面中需要使用到的数据，键值对
        host: 'http://127.0.0.1:8000',
        sections: [],  // 版块
        num_section: 0,  // 版块点击切换样式的变量
        comments: [],  // 新闻评论
        news: {},  // 新闻详情
        user_id:sessionStorage.user_id || localStorage.user_id,
        token:sessionStorage.token || localStorage.token,
        user_name:sessionStorage.username || localStorage.username,
        num_box: 0,  // 点击回复，显示回复框
        show: true,
        context: '',  //　评论的评论内容
        value: '',  // 评论新闻的内容
        box_show: 0,
    },
    computed: {
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted: function () {
        this.get_news();
        this.get_section();
        this.get_comment();
    },
    methods: {
        // 获取查询字符串
        get_query_string: function (name, def_val) {
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return def_val;
        },
        // 首页.获取版块
        get_section: function () {
            axios.get(this.host + '/sections/')
                .then(response => {
                    this.sections = response.data;
                })
                .catch(error => {
                    alert('获取版块名出错!')
                });
        },
        // 首页.版块点击，切换样式
        change_section_class: function(index) {
            this.num_section = index;
        },
        // 获取新闻详情
        get_news: function(){
            this.news_id = this.get_query_string("news_id", 1);
            axios.get(this.host + '/news/' + this.news_id)
                .then(response => {
                    this.news = response.data
                })
                .catch(error => {
                    alert('获取新闻详细信息失败!')
                })
        },
        // 获取新闻的评论
        get_comment:function(){
           axios.get(this.host + '/comments/?news_id=' + this.news_id)
                .then(response => {
                    this.comments = response.data
                })
                .catch(error => {
                    alert('获取新闻详细信息失败!')
                })
        },
        // 添加评论
        answer_news: function(news_id, comment_id){
            this.comment_text = this.context || this.value;
            axios.post(this.host + '/news_comment/', {
                news_id: news_id,
                parent_id: comment_id,
                comment: this.comment_text,
                user_id: this.user_id
            },
                {
                    headers: {
                        'Authorization': 'JWT ' + this.token
                    },
                    responseType: 'json'
                })
                .then(response => {
                    location.reload()
                })
                .catch(error => {
                    alert('添加新闻评论时出错')
                })
        },
        // 显示评论框
        show_comment_box: function(num_box){
            this.context = '';
            this.value = '';
            this.num_box = num_box
        },
        // 显示评论框
        hide_comment_box: function(num_box){
            this.context = '';
            this.value = '';
            this.num_box = num_box
        },
        // 鼠标划入头像，出现发布栏
        show_box: function(num){
            this.box_show = num
        },
        // 鼠标划出头像，隐藏发布栏
        hide_box: function(num){
            this.box_show = num
        }　


    }
})






