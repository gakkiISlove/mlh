var vm = new Vue({
    el: '#app',
    data: {
        host,  //host:host
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        companys: [],
        spits: [],
        news_list: [],
        archive_id:0,
        archives:[],
        gender:0,
        num: 0,
    },

    mounted: function () {
        this.get_archives();
        this.get_collection_spit();
        this.get_collection_news();
        this.get_collection_company();

    },

    methods: {
        // 退出
        logout(){
            sessionStorage.clear();
            localStorage.clear();
            location.href = '/login.html';
        },

        // 获取用户档案id
        get_archives: function () {
            axios.get(this.host + '/archives/', {

                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
            })
                .then(response => {
                    this.archive_id = response.data.id;
                    this.get_user_home();
                })
                .catch(error => {
                    alert(error.response.status);
                    console.log(error.response.status)
                })
        },


        // 获取用户档案信息
        get_user_home: function () {
            axios.get(this.host + '/homes/' + this.archive_id +"/", {

                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
            })
            .then(response => {
                this.archives = response.data;

                this.gender = this.archives.gender
            })
            .catch(error => {
                alert(error.response.status);
                console.log(error.response.status)
            })
        },

        // 获取用户关注吐槽
        get_collection_spit: function () {
            axios.get(this.host + '/collectionspits/', {

                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
            })
            .then(response => {
                this.spits = response.data;

            })
            .catch(error => {
                alert(error.response.status);
                console.log(error.response.status)
            })
        },

         // 获取用户关注公司
        get_collection_company: function () {
            axios.get(this.host + '/collectioncompanys/', {

                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
            })
            .then(response => {
                this.companys = response.data;

            })
            .catch(error => {
                alert(error.response.status);
                console.log(error.response.status)
            })
        },

         // 获取用户问题
        get_collection_news: function () {
            axios.get(this.host + '/collectionnews/', {

                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
            })
            .then(response => {
                this.news_list = response.data;

            })
            .catch(error => {
                alert(error.response.status);
                console.log(error.response.status)
            })
        },


    }

});

