var vm = new Vue({
    el: '#app',
    data: {
        host,  //host:host
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        questions: [],
        archive_id:0,
        archives:[],
        gender:0,
        email: "",
        mobile:"",
        password: "",
        styleObject1: {
            display: 'none',
        },
        styleObject2: {
            display: 'none',
        },
        styleObject3: {
            display: 'none',
        },
        styleObject4: {
            display: 'none',
        },
        styleObject5: {
            display: 'none',
        },


    },

    mounted: function () {
        this.get_archives();
        this.get_user_question();

    },

    methods: {
        // 退出
        logout(){
            sessionStorage.clear();
            localStorage.clear();
            location.href = '/login.html';
        },

        // 获取用户档案id
        get_archives: function () {
            axios.get(this.host + '/archives/', {

                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
            })
                .then(response => {
                    this.archive_id = response.data.id;
                    this.get_user_home();
                })
                .catch(error => {
                    alert(error.response.status);
                    console.log(error.response.status)
                })
        },


        // 获取用户档案信息
        get_user_home: function () {
            axios.get(this.host + '/homes/' + this.archive_id +"/", {

                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
            })
            .then(response => {
                this.archives = response.data;
                this.gender = this.archives.gender;
                this.username = this.archives.user.username;
                this.mobile = this.archives.user.mobile;
                this.password = this.archives.user.password;


                // this.my_web_url = this.archives.my_web_url;
                this.email = this.archives.email;
                // this.address = this.archives.address;
                // this.user_introduction = this.archives.user_introduction;
                // this.university = this.archives.university;
                // this.organization = this.archives.organization;
                // this.avatar_url = this.archives.avatar_url;
                // this.complete_degree = this.archives.complete_degree;
                // this.jobexperience_set = this.archives.jobexperience_set;
                // this.educationexperience_set = this.archives.educationexperience_set;
                // this.userteachnical_set = this.archives.userteachnical_set;
                // this.city = this.archives.city;

            })
            .catch(error => {
                alert(error.response.status);
                console.log(error.response.status)
            })
        },

        // 获取用户问题
        get_user_question: function () {
            axios.get(this.host + '/userquestions/', {

                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
            })
            .then(response => {
                this.questions = response.data;

            })
            .catch(error => {
                alert(error.response.status);
                console.log(error.response.status)
            })
        },

        // 修改档案
        modify_archives1: function () {
            alert(this.archives.real_name)
            this.styleObject1={
                display: 'none',
                };
            axios.patch(this.host + '/useralter/',
                {
                    real_name : this.archives.real_name,
                },
                {
                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
            })
            .then(response => {
                this.archives = response.data;
                 window.location.reload()
            })
            .catch(error => {
                alert(error.response.status);
                console.log(error.response.status)
            })
        },

        // 修改档案
        modify_archives2: function () {

            this.styleObject2={
                display: 'none',
                };
            axios.patch(this.host + '/useralter/',
                {
                    my_web_url : this.archives.my_web_url,
                },
                {
                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
            })
            .then(response => {
                this.archives = response.data;
                 window.location.reload()
            })
            .catch(error => {
                alert(error.response.status);
                console.log(error.response.status)
            })
        },

        // 修改style属性
        modify_archives3: function () {
            this.styleObject3={
                display: 'block',
                };
            axios.patch(this.host + '/useralter/',
                {
                    email : this.archives.email,
                },
                {
                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
            })
            .then(response => {
                this.archives = response.data;
                window.location.reload()
            })
            .catch(error => {
                alert(error.response.status);
                console.log(error.response.status)
            })

        },

        // 修改档案
        modify_archives4: function () {
            this.styleObject4={
                display: 'block',
                };
            axios.patch(this.host + '/useralter/',
                {
                    phone : this.archives.phone,
                },
                {
                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
            })
            .then(response => {
                this.archives = response.data;
                window.location.reload()
            })
            .catch(error => {
                alert(error.response.status);
                console.log(error.response.status)
            })

        },



        // 修改style属性
        modify_style1: function () {
            this.styleObject1={
                display: 'block',
                };
        },

        // 修改style属性
        modify_style2: function () {
            this.styleObject1={
                display: 'block',
                };
        },

        // 修改style属性
        modify_style3: function () {
            this.styleObject3={
                display: 'block',
                };
        },

        // 修改style属性
        modify_style4: function () {
            this.styleObject4={
                display: 'block',
                };
        },

         modify_style5: function () {
            this.styleObject5={
                display: 'block',
                };
        },
    }

});

