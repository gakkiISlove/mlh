from django.shortcuts import render
from rest_framework.viewsets import ReadOnlyModelViewSet
from .models import Activity
from . import serializers


class ActivityViewSet(ReadOnlyModelViewSet):

    queryset = Activity.objects.all()

    def get_serializer_class(self):
        if self.action == "list":
            return serializers.ActivityIndexSerializer
        else:
            return serializers.ActivityDetailSerializer
