
from rest_framework.generics import ListAPIView, GenericAPIView
from rest_framework.mixins import RetrieveModelMixin
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from .serializers import SpitSerializer, SpitCommentSeriazer
from .models import Spit, SpitComment


class SpitList(APIView):
    """吐槽列表"""
    def get(self, request):
        spits = Spit.objects.all()
        serializer = SpitSerializer(spits, many=True)
        return Response(serializer.data)


class SpitDetail(APIView):
    """吐槽详情"""

    def get(self, request):
        spit_id = request.query_params.get('spit_id')
        spit = Spit.objects.get(id=spit_id)
        serializer = SpitSerializer(spit)
        return Response(serializer.data)


class Spit_Comment(GenericViewSet):
    """吐槽评论"""
    queryset = SpitComment.objects.all()
    serializer_class = SpitCommentSeriazer

    def get_queryset(self):
        spit_id = self.request.query_params.get('spit_id')
        return SpitComment.objects.filter(spit_id=spit_id)

    def list(self, request):
        comment = self.get_queryset()
        serializer = self.get_serializer(comment, many=True)
        return Response(serializer.data)
