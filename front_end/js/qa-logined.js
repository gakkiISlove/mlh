let apps = new Vue({
    el:"#apps",
    data:{
        // 页面中需要使用到的数据，键值对
        data:[],
        s:[],
        num : 1,
        ban:1,
        result_a:[],
        user_id:sessionStorage.user_id || localStorage.user_id,
        user_name:sessionStorage.username || localStorage.username,
        host: host,
        error_username: false,
        error_pwd: false,
        error_pwd_message: '请填写密码',
        username: '',
        password: '',
        remember: false,
        now:0,
    },
    computed:{
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted:function () {
        // 一加载就需要做的，直接是代码
        // 展示所有问答
        num = this.get_query_string('id');
        if (num == null){
            this.ban=1
            axios.get(host+'/language/')

            .then(response=>{
                console.info(response.data)
                this.data = response.data['data']
                this.s = response.data['s']

                this.result_a = response.data['result_a']
                // alert(this.result_a )
                let now = response.headers.date
                now = new Date(now)
                console.log(now);
                this.now = now.getTime()
            })
            .catch(error =>{
                alert('出错了')
            })
        }
        else {
            this.ban =num
            this.Banner(this.ban)
        }



        function count_last_time(datatime){

        };

    },
    methods:{
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
        // 点击语言展示
        Banner: function (value) {
            if (value>10){
                this.ban = 1
                value = 1
                }

            axios.get(host+'/language?id='+value+'&new='+this.num,{
                responseType: 'json',
            })
            .then(response=>{
                console.info(response.data)
                this.data = response.data['data']
                this.s = response.data['s']
                this.result_a = response.data['result_a']

                this.ban = value
                this.num = this.num
                let now = response.headers.date
                now = new Date(now)
                console.log(now);
                this.now = now.getTime()

            })
            .catch(error =>{
                alert('出错了111')
            })
        },
        list:function (value) {
            // 点击问答排序的条件展示
            axios.get(host+'/sorting?new='+value+'&id='+this.ban,{
                responseType: 'json',
            })
            .then(response=>{
                console.info(response.data)
                this.s = response.data['s']
                this.num = value
            })
            .catch(error =>{
                alert('出错了')
            })
        },
        // 获取url参数
        get_query_string: function(name){
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        },
        // 检查数据
        check_username: function () {
            if (!this.username) {
                this.error_username = true;
            } else {
                this.error_username = false;
            }
        },
        check_pwd: function () {
            if (!this.password) {
                this.error_pwd_message = '请填写密码';
                this.error_pwd = true;
            } else {
                this.error_pwd = false;
            }
        },
        // 表单提交
        on_submit: function () {
            this.check_username();
            this.check_pwd();

            if (this.error_username == false && this.error_pwd == false) {
                axios.post(this.host + '/authorizations/', {
                    username: this.username,
                    password: this.password
                }, {
                    responseType: 'json',
                    withCredentials: true
                })
                    .then(response => {
                        // 使用浏览器本地存储保存token
                        if (this.remember) {
                            // 记住登录
                            sessionStorage.clear();
                            localStorage.token = response.data.token;
                            localStorage.user_id = response.data.user_id;
                            localStorage.username = response.data.username;
                        } else {
                            // 未记住登录
                            localStorage.clear();
                            sessionStorage.token = response.data.token;
                            sessionStorage.user_id = response.data.user_id;
                            sessionStorage.username = response.data.username;
                        }

                        // 跳转页面
                        var return_url = this.get_query_string('next');
                        if (!return_url) {
                            return_url = './qa-logined.html';
                            console.log("meiyou")
                        }
                        console.log(return_url);
                        location.href = return_url;
                    })
                    .catch(error => {
                        if (error.response.status == 400) {
                            this.error_pwd_message = '用户名或密码错误';
                        } else {
                            this.error_pwd_message = '服务器错误';
                        }
                        this.error_pwd = true;
                    })
            }
        },

        // 获取最新一次回答提交到现在有几分钟
        count_last_time:function (datatime) {
            create_time = new Date(datatime)
            create_count = create_time.getTime()
            last_count = this.now - create_count
            return parseInt(last_count / 60 / 1000)
        }

    }

});




