var vm = new Vue({
    el: '#app',
    data: {
        host,  //host:host
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        question_shares: [],
        archive_id:0,
        archives:[],
        gender:0,
    },

    mounted: function () {
        this.get_archives();
        this.get_question_share();

    },

    methods: {
        // 退出
        logout(){
            sessionStorage.clear();
            localStorage.clear();
            location.href = '/login.html';
        },

        // 获取用户档案id
        get_archives: function () {
            axios.get(this.host + '/archives/', {

                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
            })
                .then(response => {
                    this.archive_id = response.data.id;
                    this.get_user_home();
                })
                .catch(error => {
                    alert(error.response.status);
                    console.log(error.response.status)
                })
        },


        // 获取用户档案信息
        get_user_home: function () {
            axios.get(this.host + '/homes/' + this.archive_id +"/", {

                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
            })
            .then(response => {
                this.archives = response.data;

                this.gender = this.archives.gender
            })
            .catch(error => {
                alert(error.response.status);
                console.log(error.response.status)
            })
        },

        // 获取用户问题
        get_question_share: function () {
            axios.get(this.host + '/questionshare/', {

                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
            })
            .then(response => {
                this.question_shares = response.data;

            })
            .catch(error => {
                alert(error.response.status);
                console.log(error.response.status)
            })
        },


    }

});
