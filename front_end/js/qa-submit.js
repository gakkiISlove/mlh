var apps = new Vue({
    el:"#apps",
    data:{
        // 页面中需要使用到的数据，键值对
        categroies:[],
        select:"",
        contents:"",
        title:"",
        label_id:"",
        label:'',
        labels:[],
        select_label:'',
        user_id:sessionStorage.user_id || localStorage.user_id,
        user_name:sessionStorage.username || localStorage.username,
    },

    computed:{
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted:function () {
        // 一加载就需要做的，直接是代码
        // 语言展示
         axios.get(host+'/customtag/',{
            responseType:'json'
            })
            .then(response=>{
                this.categroies = response.data['categroy']
            })
            .catch(error =>{
                alert('出错了')
            })
    },
    watch:{
        select:function () {
            // 根据语言id查询关联的标签
            axios.get(host+'/customtag/',{
            responseType:'json'
            }).then(response=>{
                this.label = response.data['categroy']
                this.labels = this.label[this.select-1].label_set

            })
            .catch(error =>{
                alert('出错了')
            })
        },
        select_label:function(){

        }
    },
    methods:{
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数

        // 点击提交问题
        send_question: function () {
            this.contents = CKEDITOR.instances.masssage.getData();
            if (this.contents&&this.user_id&&this.title&&this.select&&this.select_label)
            {
                axios.post(host + '/submit/',{
                    // 通过axios将所获得的数据传送到后端
                    content: CKEDITOR.instances.masssage.document.getBody().getText(),
                    user: this.user_id,
                    title:this.title,
                    language_category:this.select,
                    label:this.select_label,
                } ,{
                responseType:'json'
                }).then(response => {
                    alert('提交成功')
                    window.location.href="./qa-logined.html"

                })
                .catch(error => {
                    console.log(error.response.data)

                });
            }

            else {
                alert('请输入完整的数据')
            }
        }
    }
})