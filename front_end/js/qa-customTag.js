/**
 * Created by python on 18-11-10.
 */

var apps = new Vue({
    el:"#apps",
    data:{
        // 页面中需要使用到的数据，键值对
        categroies:[],
        labels:[],
        num : 1,
        ban:1,
        categroy_id:'',
        select:"",
        txt:'',
        user_id:sessionStorage.user_id || localStorage.user_id,
        user_name:sessionStorage.username || localStorage.username,
    },
    computed:{
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted:function () {
        // 一加载就需要做的，直接是代码
        axios.get(host+"/customtag/")
            .then(response=>{
                // alert(1)
                console.info(response.data)

                this.categroies = response.data['categroy']
            })
            .catch(error =>{
                alert('出错了')
            })
        },
    watch:{
        select:function () {
            // 根据语言id查询是否被关注
            if (this.user_id) {
                axios.post(host + '/queryfollow/', {
                    // 通过axios将所获得的数据传送到后端
                    uid: this.user_id,  //用户id
                    lid: this.select,    // 语言的id
                }, {
                    responseType: 'json'
                }).then(response => {

                    if (response.data['massage'] == '已关注'){
                        this.txt=true
                    }else {
                        this.txt=false
                    }
                })
                    .catch(error => {
                        console.log(error.response.data)
                    });
            } else {
                alert('请先登陆')
            }
        }
    },
    methods:{
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
        Banner: function (value) {
            // 语言类别导航条跳转
            axios.get(host+'/language?id='+value+'&new='+this.num,{
                responseType: 'json',
            })
            .then(response=>{
                console.info(response.data)
                this.s = response.data['s']
                this.ban = value
                this.num = this.num
                window.location.href='./qa-logined.html?id='+this.ban+'&new='+this.num
            })
            .catch(error =>{
                alert('出错了111')
            })
        },

        addfollow:function (select) {
            //添加关注
            if (this.user_id){
                 this.language_id = select,
                axios.post(host + '/addfollow/', {
                     // 通过axios将所获得的数据传送到后端
                     uid: this.user_id,  //用户id
                     lid: this.language_id,    // 回答的内容
                 }, {
                     responseType: 'json'
                 }).then(response => {

                    location.reload()
                 })
                     .catch(error => {
                         console.log(error.response.data)
                 });
            }else {
                alert('请先登陆')
            }

        },

        delfollow:function (select) {
            //取消关注
            if (this.user_id){
                 this.language_id = select,
                axios.post(host + '/delfollow/', {
                     // 通过axios将所获得的数据传送到后端
                     uid: this.user_id,  //用户id
                     lid: this.language_id,    // 回答的内容
                 }, {
                     responseType: 'json'
                 }).then(response => {

                    location.reload()
                 })
                     .catch(error => {
                         console.log(error.response.data)
                 });
            }else {
                alert('请先登陆')
            }

        },
    }
})